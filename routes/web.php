<?php

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProveedoresController;
use App\Http\Controllers\SaleDetailsController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\ShoppingDetailsController;
use App\Http\Controllers\ShoppingsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');


// MODULO DE USUARIOS
// Route::group(['middleware' => ['permission:users.admin']], function () {
    Route::get('/users', [UserController::class, 'index'])->name('users.index')->middleware('can:users.admin');
    Route::get('/users_create', [UserController::class, 'create'])->name('users.create')->middleware('can:users.admin');
    Route::post('/users_new',[UserController::class, 'store'])->name('users.store')->middleware('can:users.admin');
    Route::get('/users_modal_state/{id}',[UserController::class, 'modalState'])->name('users.modalState')->middleware('can:users.admin');
    Route::post('/users_change_state/{id}',[UserController::class, 'updateState'])->name('users.updateState')->middleware('can:users.admin');
    Route::get('/users_modal_update/{id}',[UserController::class, 'modalUpdate'])->name('users.modalUpdate')->middleware('can:users.admin');
    Route::post('/users_update/{id}',[UserController::class, 'update'])->name('users.update')->middleware('can:users.admin');
    Route::get('/users_modal_destroy/{id}',[UserController::class, 'modalDestroy'])->name('users.modalDestroy')->middleware('can:users.admin');
    Route::post('/users_destroy/{id}',[UserController::class, 'destroy'])->name('users.destroy')->middleware('can:users.admin');
// });

// MODULO DE PROVEEDORES
Route::get('/providers', [ProveedoresController::class, 'index'])->name('providers.index')->middleware('can:providers.admin');
Route::get('/providers/create', [ProveedoresController::class, 'create'])->name('providers.create')->middleware('can:providers.admin');
Route::post('/providers/store',[ProveedoresController::class, 'store'])->name('providers.store')->middleware('can:providers.admin');
Route::get('/providers/modal_update/{id}',[ProveedoresController::class, 'modalUpdate'])->name('providers.modalUpdate')->middleware('can:providers.admin');
Route::post('/providers/update/{id}',[ProveedoresController::class, 'update'])->name('providers.update')->middleware('can:providers.admin');
Route::get('/providers/modal_state/{id}',[ProveedoresController::class, 'modalState'])->name('providers.modalState')->middleware('can:providers.admin');
Route::post('/providers/update_state/{id}',[ProveedoresController::class, 'updateState'])->name('providers.updateState')->middleware('can:providers.admin');
Route::get('/providers/modal_destroy/{id}',[ProveedoresController::class, 'modalDestroy'])->name('providers.modalDestroy')->middleware('can:providers.admin');
Route::post('/providers/destroy/{id}',[ProveedoresController::class, 'destroy'])->name('providers.destroy')->middleware('can:providers.admin');

// MODULO DE CLIENTES
Route::get('/clients', [ClientsController::class, 'index'])->name('clients.index')->middleware('can:clients.admin');
Route::get('/clients/create', [ClientsController::class, 'create'])->name('clients.create')->middleware('can:clients.admin');
Route::post('/clients/store',[ClientsController::class, 'store'])->name('clients.store')->middleware('can:clients.admin');
Route::get('/clients/modal_state/{id}',[ClientsController::class, 'modalState'])->name('clients.modalState')->middleware('can:clients.admin');
Route::post('/clients/update_state/{id}',[ClientsController::class, 'updateState'])->name('clients.updateState')->middleware('can:clients.admin');
Route::get('/clients/modal_destroy/{id}',[ClientsController::class, 'modalDestroy'])->name('clients.modalDestroy')->middleware('can:clients.admin');
Route::post('/clients/destroy/{id}',[ClientsController::class, 'destroy'])->name('clients.destroy')->middleware('can:clients.admin');
Route::get('/clients/modal_update/{id}',[ClientsController::class, 'modalUpdate'])->name('clients.modalUpdate')->middleware('can:clients.admin');
Route::post('/clients/update/{id}',[ClientsController::class, 'update'])->name('clients.update')->middleware('can:clients.admin');


// MODULO DE PRODUCTOS
Route::get('/products', [ProductsController::class, 'index'])->name('products.index')->middleware(['can:clients.admin']); 
Route::get('/products/create', [ProductsController::class, 'create'])->name('products.create')->middleware('can:products.admin');
Route::post('/products/store',[ProductsController::class, 'store'])->name('products.store')->middleware('can:products.admin');
Route::get('/products/modal_update/{id}',[ProductsController::class, 'modalUpdate'])->name('products.modalUpdate')->middleware('can:products.admin');
Route::post('/products/update/{id}',[ProductsController::class, 'update'])->name('products.update')->middleware('can:products.admin');

Route::get('/products/modal_state/{id}',[ProductsController::class, 'modalState'])->name('products.modalState')->middleware('can:products.admin');
Route::post('/products/update_state/{id}',[ProductsController::class, 'updateState'])->name('products.updateState')->middleware('can:products.admin');
Route::get('/products/modal_destroy/{id}',[ProductsController::class, 'modalDestroy'])->name('products.modalDestroy')->middleware('can:products.admin');
Route::post('/products/destroy/{id}',[ProductsController::class, 'destroy'])->name('products.destroy')->middleware('can:products.admin');


// MODULO DE COMPRAS
Route::get('/shoppings', [ShoppingsController::class, 'index'])->name('shoppings.index')->middleware('can:shopping.admin');
Route::get('/shoppings/create', [ShoppingsController::class, 'create'])->name('shoppings.create')->middleware('can:shopping.admin');
Route::post('/shoppings/store',[ShoppingsController::class, 'store'])->name('shoppings.store')->middleware('can:shopping.admin');
Route::get('/shoppings/show/{id}', [ShoppingsController::class, 'show'])->name('shoppings.show')->middleware('can:shopping.admin');
Route::get('/shoppings/modal_update/{id}',[ShoppingsController::class, 'modalUpdate'])->name('shoppings.modalUpdate')->middleware('can:shopping.admin');
Route::post('/shoppings/update/{id}',[ShoppingsController::class, 'update'])->name('shoppings.update')->middleware('can:shopping.admin');
Route::get('/shoppings/modal_destroy/{id}',[ShoppingsController::class, 'modalDestroy'])->name('shoppings.modalDestroy')->middleware('can:shopping.admin');
Route::post('/shoppings/destroy/{id}',[ShoppingsController::class, 'destroy'])->name('shoppings.destroy')->middleware('can:shopping.admin');
Route::get('/shoppings/modal_state/{id}',[ShoppingsController::class, 'modalState'])->name('shoppings.modalState')->middleware('can:shopping.admin');
Route::post('/shoppings/update_state/{id}',[ShoppingsController::class, 'updateState'])->name('shoppings.updateState')->middleware('can:shopping.admin');

// DETALLES DE COMPRAS
Route::get('/shopping_details/modal_create/{id}',[ShoppingDetailsController::class, 'modalCreate'])->name('shopping.details.modalCreate')->middleware('can:shopping.admin');
Route::post('/shopping_details/store/{id}',[ShoppingDetailsController::class, 'store'])->name('shopping.details.store')->middleware('can:shopping.admin');
Route::get('/shopping_details/modal_update/{id}',[ShoppingDetailsController::class, 'modalUpdate'])->name('shopping.details.modalUpdate')->middleware('can:shopping.admin');
Route::post('/shopping_details/update/{id}',[ShoppingDetailsController::class, 'update'])->name('shopping.details.update')->middleware('can:shopping.admin');
Route::get('/shopping_details/modal_destroy/{id}',[ShoppingDetailsController::class, 'modalDestroy'])->name('shopping.details.modalDestroy')->middleware('can:shopping.admin');
Route::post('/shopping_details/destroy/{id}',[ShoppingDetailsController::class, 'destroy'])->name('shopping.details.destroy')->middleware('can:shopping.admin');


// MODULO DE VENTAS
Route::get('/sales', [SalesController::class, 'index'])->name('sales.index')->middleware('can:clients.admin');
Route::get('/sales/create', [SalesController::class, 'create'])->name('sales.create')->middleware('can:clients.admin');
Route::post('/sales/store',[SalesController::class, 'store'])->name('sales.store')->middleware('can:clients.admin');
Route::get('/sales/show/{id}', [SalesController::class, 'show'])->name('sales.show')->middleware('can:clients.admin');
Route::get('/sales/modal_update/{id}',[SalesController::class, 'modalUpdate'])->name('sales.modalUpdate')->middleware('can:clients.admin');
Route::post('/sales/update/{id}',[SalesController::class, 'update'])->name('sales.update')->middleware('can:clients.admin');
Route::get('/sales/modal_destroy/{id}',[SalesController::class, 'modalDestroy'])->name('sales.modalDestroy')->middleware('can:clients.admin');
Route::post('/sales/destroy/{id}',[SalesController::class, 'destroy'])->name('sales.destroy')->middleware('can:clients.admin');
Route::get('/sales/modal_state/{id}',[SalesController::class, 'modalState'])->name('sales.modalState')->middleware('can:clients.admin');
Route::post('/sales/update_state/{id}',[SalesController::class, 'updateState'])->name('sales.updateState')->middleware('can:clients.admin');

// DETALLES DE VENTAS
Route::get('/sale_details/modal_create/{id}',[SaleDetailsController::class, 'modalCreate'])->name('sale.details.modalCreate')->middleware('can:clients.admin');
Route::post('/sale_details/store/{id}',[SaleDetailsController::class, 'store'])->name('sale.details.store')->middleware('can:clients.admin');
Route::get('/sale_details/modal_update/{id}',[SaleDetailsController::class, 'modalUpdate'])->name('sale.details.modalUpdate')->middleware('can:clients.admin');
Route::post('/sale_details/update/{id}',[SaleDetailsController::class, 'update'])->name('sale.details.update')->middleware('can:clients.admin');
Route::get('/sale_details/modal_destroy/{id}',[SaleDetailsController::class, 'modalDestroy'])->name('sale.details.modalDestroy')->middleware('can:clients.admin');
Route::post('/sale_details/destroy/{id}',[SaleDetailsController::class, 'destroy'])->name('sale.details.destroy')->middleware('can:clients.admin');
