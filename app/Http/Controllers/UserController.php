<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name','asc')->get();
        // Session::put('item', '13.');
        return view('auth.index', compact('users'));
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        // Session::put('item', '13.');
        return view('auth.register', compact('roles'));
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required'  => 'El campo nombre es obligatorio.',
            'name.max'  => 'El campo nombre no debe contener más de 255 caracteres.',
            'email.required'  => 'El campo correo electrónico es obligatorio.',
            'email.email'  => 'El campo  correo electrónico debe ser una dirección de correo válida.',
            'email.max'  => 'El campo  correo electrónico no debe contener más de 255 caracteres.',
            'password.required'  => 'El campo contraseña es obligatorio.',
            'password.min'  => 'El campo contraseña debe tener al menos 8 caracteres.',
            'password.confirmed'  => 'Los campos de contraseña no son iguales.',
            'last_name.required'  => 'El campo apellido es obligatorio.',
            'last_name.max'  => 'El campo apellido no debe contener más de 255 caracteres.',
            'last_name.min'  => 'El campo apellido debe tener al menos 2 caracteres.',
            'phone.required'  => 'El campo teléfono es obligatorio.',
            'phone.max'  => 'El campo teléfono no debe contener más de 8 dígitos.',
            'phone.min'  => 'El campo teléfono debe tener al menos 7 dígitos.',
            'phone.integer'  => 'El campo teléfono debe ser numérico',
            'city.required'  => 'El campo ciudad es obligatorio.',
            'city.max'  => 'El campo ciudad no debe contener más de 255 caracteres.',
            'city.min'  => 'El campo ciudad debe tener al menos 2 caracteres.',
            'address.required'  => 'El campo dirección es obligatorio.',
            'address.max'  => 'El campo dirección no debe contener más de 255 caracteres.',
            'address.min'  => 'El campo dirección debe tener al menos 5 caracteres.',

            'role.required'  => 'El campo rol es obligatorio.', 
        ];
        $validateArray = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email:filter', 'max:255', Rule::unique('users', 'email')],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'last_name' => ['required', 'string', 'min:2', 'max:255'],
            'phone' => ['required', 'integer', 'min:1000000', 'max:99999999'],
            'city' => ['required', 'string', 'min:2', 'max:255'],
            'address' => ['required', 'string', 'min:5', 'max:255'],
            'role' => ['required']
        ];
        $request->validate($validateArray, $messages);

        $role = Role::find($request->role);
        $role = (isset($role->id))? $role :  Role::find(1);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'city' => $request->city,
            'address' => $request->address,
            'state' => '1',
        ])->assignRole($role);

        toastr()->success('Creado con éxito.','Usuario '.$user->name.' '.$user->last_name, ['positionClass' => 'toast-bottom-right',]);
        return Redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalState($id)
    {
        $user = User::findOrFail($id);
        return view('auth.modal_state', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState($id)
    {
        $user = User::findOrFail($id);
        $user->state = ($user->state == '1')? '2' : '1';
        $user->update();
        if ($user->state == '1') {
            toastr()->success('Activado con éxito.','Usuario '.$user->name.' '.$user->last_name, ['positionClass' => 'toast-bottom-right',]);
        } else {
            toastr()->success('Inactivado con éxito.','Usuario '.$user->name.' '.$user->last_name, ['positionClass' => 'toast-bottom-right',]);
        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $roles = Role::all();
        $user = User::findOrFail($id);
        return view('auth.modal_update', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $messages = [
            'name.required'  => 'El campo nombre es obligatorio.',
            'name.max'  => 'El campo nombre no debe contener más de 255 caracteres.',
            'email.required'  => 'El campo correo electrónico es obligatorio.',
            'email.email'  => 'El campo  correo electrónico debe ser una dirección de correo válida.',
            'email.max'  => 'El campo  correo electrónico no debe contener más de 255 caracteres.',
            'password.required'  => 'El campo contraseña es obligatorio.',
            'password.min'  => 'El campo contraseña debe tener al menos 8 caracteres.',
            'password.confirmed'  => 'Los campos de contraseña no son iguales.',
            'last_name.required'  => 'El campo apellido es obligatorio.',
            'last_name.max'  => 'El campo apellido no debe contener más de 255 caracteres.',
            'last_name.min'  => 'El campo apellido debe tener al menos 2 caracteres.',
            'phone.required'  => 'El campo teléfono es obligatorio.',
            'phone.max'  => 'El campo teléfono no debe contener más de 8 dígitos.',
            'phone.min'  => 'El campo teléfono debe tener al menos 7 dígitos.',
            'phone.integer'  => 'El campo teléfono debe ser numérico',
            'city.required'  => 'El campo ciudad es obligatorio.',
            'city.max'  => 'El campo ciudad no debe contener más de 255 caracteres.',
            'city.min'  => 'El campo ciudad debe tener al menos 2 caracteres.',
            'address.required'  => 'El campo dirección es obligatorio.',
            'address.max'  => 'El campo dirección no debe contener más de 255 caracteres.',
            'address.min'  => 'El campo dirección debe tener al menos 5 caracteres.',

            'role.required'  => 'El campo rol es obligatorio.', 
        ];
        $validateArray = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email:filter', 'max:255', Rule::unique('users', 'email')->ignore($id)],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
            'last_name' => ['required', 'string', 'min:2', 'max:255'],
            'phone' => ['required', 'integer', 'min:1000000', 'max:99999999'],
            'city' => ['required', 'string', 'min:2', 'max:255'],
            'address' => ['required', 'string', 'min:5', 'max:255'],
            'role' => ['required']
        ];
        $request->validate($validateArray, $messages);
       
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->update();

        $role = Role::find($request->role);
        $role = (isset($role->id))? $role :  Role::find(1);
        $user->assignRole($role);

        toastr()->success('Modificado con éxito.','Usuario '.$user->name.' '.$user->last_name, ['positionClass' => 'toast-bottom-right',]);
        return Response::json(['success' => '1']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $user = User::findOrFail($id);
        return view('auth.modal_delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        toastr()->success('Eliminado con éxito.','Usuario '.$user->name.' '.$user->last_name, ['positionClass' => 'toast-bottom-right',]);
        return back();
    }
}
