<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::check('products.admin')) {
            $products = Product::orderBy('name','asc')->get();
        } else {
            $products = Product::orderBy('name','asc')->where('state','1')->get();

        }
        return view('products.index', compact('products'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['pushopping'] = monedaVal($request->pushopping);
        $request['pusale'] = monedaVal($request->pusale);
        $request['stock'] = monedaVal($request->stock);
     
        $messages = [
            'name.required'  => 'El campo nombre del producto es obligatorio.',
            'name.max'  => 'El campo nombre del producto no debe contener más de 255 caracteres.',
            'pushopping.required'  => 'El campo precio de compra es obligatorio.',
            'pushopping.max'  => 'El campo  precio de compra no debe ser mayor a 9999999999.99.',
            'pushopping.min'  => 'El campo  precio de compra debe ser al menos 0.',
            'pushopping.numeric'  => 'El campo precio de compra debe ser numérico.',
            'pusale.required'  => 'El campo precio de venta es obligatorio.',
            'pusale.max'  => 'El campo  precio de venta no debe ser mayor a 9999999999.99.',
            'pusale.min'  => 'El campo  precio de venta debe ser al menos '.$request->pushopping,
            'pusale.numeric'  => 'El campo precio de venta debe ser numérico.',
            'description.max'  => 'El campo más información no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'name' => ['required', 'string', 'max:200'],
            'marca' => ['required', 'string', 'max:100'],
            'talla' => ['required', 'string', 'max:45'],
            'color' => ['required', 'string', 'max:45'],
            'stock' => ['required', 'integer', 'min:0', 'max:9999999999'],

            'pushopping' => ['required', 'numeric', 'min:0', 'max:9999999999.99'],
            'pusale' => ['required', 'numeric', 'min:'.$request->pushopping, 'max:9999999999.99'],
            'description' => ['nullable','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $maximo=Product::select('code')->max('code');
        $code = generateCode($maximo,'PRO0001','PRO',3,4);

        $product = Product::create([
            'code' => $code,
            'name' => $request->name,
            'marca' => $request->marca,
            'talla' => $request->talla,
            'color' => $request->color,
            'stock' => $request->stock,
            'pu_shopping' => $request->pushopping,
            'pu_sale' => $request->pusale,
            'description' => $request->description,
            'state' => '1',
            'user_id' => Auth::user()->id,
        ]);

        toastr()->success('Creado con éxito.','Producto '.$product->name, ['positionClass' => 'toast-bottom-right',]);
        return Redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $product = Product::findOrFail($id);
        return view('products.modal_update', compact('product'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['pushopping'] = monedaVal($request->pushopping);
        $request['pusale'] = monedaVal($request->pusale);
        $request['stock'] = monedaVal($request->stock);
        
        $messages = [
            'name.required'  => 'El campo nombre del producto es obligatorio.',
            'name.max'  => 'El campo nombre del producto no debe contener más de 255 caracteres.',
            'pushopping.required'  => 'El campo precio de compra es obligatorio.',
            'pushopping.max'  => 'El campo  precio de compra no debe ser mayor a 9999999999.99.',
            'pushopping.min'  => 'El campo  precio de compra debe ser al menos 0.',
            'pushopping.numeric'  => 'El campo precio de compra debe ser numérico.',
            'pusale.required'  => 'El campo precio de venta es obligatorio.',
            'pusale.max'  => 'El campo  precio de venta no debe ser mayor a 9999999999.99.',
            'pusale.min'  => 'El campo  precio de venta debe ser al menos 0.',
            'pusale.numeric'  => 'El campo precio de venta debe ser numérico.',
            'description.max'  => 'El campo más información no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'name' => ['required', 'string', 'max:200'],
            'marca' => ['required', 'string', 'max:100'],
            'talla' => ['required', 'string', 'max:45'],
            'color' => ['required', 'string', 'max:45'],
            'stock' => ['required', 'integer', 'min:0', 'max:9999999999'],
            'pushopping' => ['required', 'numeric', 'min:0', 'max:9999999999.99'],
            'pusale' => ['required', 'numeric', 'min:0', 'max:9999999999.99'],
            'description' => ['nullable','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $product = Product::findOrFail($id);
        $product->name =  $request->name;
        $product->marca =  $request->marca;
        $product->talla =  $request->talla;
        $product->color =  $request->color;
        $product->stock =  $request->stock;
        $product->pu_shopping =  $request->pushopping;
        $product->pu_sale =  $request->pusale;
        $product->description =  $request->description;
        $product->update();

        toastr()->success('Modificado con éxito.','Producto '.$product->name, ['positionClass' => 'toast-bottom-right']);
        return Response::json(['success' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalState($id)
    {
        $product = Product::findOrFail($id);
        return view('products.modal_state', compact('product'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState($id)
    {
        $product = Product::findOrFail($id);
        $product->state = ($product->state == '1')? '2' : '1';
        $product->update();
        if ($product->state == '1') {
            toastr()->success('Activado con éxito.','Producto '.$product->name, ['positionClass' => 'toast-bottom-right',]);
        } else {
            toastr()->success('Inactivado con éxito.','Producto '.$product->name, ['positionClass' => 'toast-bottom-right',]);
        }
        
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $product = Product::findOrFail($id);
        return view('products.modal_delete', compact('product'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        toastr()->success('Eliminado con éxito.','Producto '.$product->name, ['positionClass' => 'toast-bottom-right',]);
        return back();
    }

}
