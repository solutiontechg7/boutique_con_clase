<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use App\Models\Proveedore;
use App\Models\Shopping;
use App\Models\ShoppingDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class ShoppingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shoppings = Shopping::orderBy('code','asc')->get();
        return view('shopping.index', compact('shoppings'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedore::where('state','1')->get();
        return view('shopping.create', compact('proveedores'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'description.required'  => 'El campo más descripción es obligatorio.',
            'description.max'  => 'El campo más descripción no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'proveedor' => ['required', Rule::exists('proveedores','id')],
            'description' => ['required','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $maximo=Shopping::select('code')->max('code');
        $code = generateCode($maximo,'C000001','C',1,6);

        $shopping = Shopping::create([
            'code' => $code,
            'proveedor_id' => $request->proveedor,
            'description' => $request->description,
            'state' => '0',
            'user_id' => Auth::user()->id,
        ]);

        toastr()->success('Creada con éxito.','Cabecera de Compra', ['positionClass' => 'toast-bottom-right',]);
        return Redirect()->route('shoppings.show',$shopping->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shopping = Shopping::findOrFail($id);
        return view('shopping.show', compact('shopping'));
    
    }
  
       /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $proveedores = Proveedore::where('state','1')->get();
        $shopping = Shopping::findOrFail($id);
        return view('shopping.modal_update', compact('shopping','proveedores'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'description.required'  => 'El campo más descripción es obligatorio.',
            'description.max'  => 'El campo más descripción no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'proveedor' => ['required', Rule::exists('proveedores','id')],
            'description' => ['required','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $shopping = Shopping::findOrFail($id);

        if($shopping->state != '0'){
            toastr()->error('No se puede modificar los datos de la compra debido a que no se encuentra pendiente.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }

        $shopping->proveedor_id =  $request->proveedor;
        $shopping->description =  $request->description;
        $shopping->update();

        toastr()->success('Modificada con éxito.','Compra '.$shopping->code, ['positionClass' => 'toast-bottom-right']);
        return Response::json(['success' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalState($id)
    {
        $shopping = Shopping::findOrFail($id);
        return view('shopping.modal_state', compact('shopping'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState(Request $request, $id)
    {
        $validateArray = [
            'estado' => ['required', 'regex:~(^[12]{1})$~'],
        ];
        $request->validate($validateArray);

        $shopping = Shopping::findOrFail($id);

        if($shopping->state != '0'){
            toastr()->error('No se puede cambiar de estado la compra debido a que no se encuentra pendiente.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }
        $shopping->state = $request->estado;
        $shopping->update();

        if ($shopping->state == '1') {
            foreach ($shopping->details as $key => $detail) {
                $product = $detail->product;
                $product->stock += $detail->quantity;
                $product->update();
            }
            $shopping->date_validation = now();
            $shopping->update();
            
            toastr()->success('Validada con éxito.','Compra '.$shopping->code, ['positionClass' => 'toast-bottom-right',]);
            toastr()->success('Actualizado exitosamente','Stock de Productos', ['positionClass' => 'toast-bottom-right',]);
        } else {
            toastr()->success('Anulada con éxito.','Compra '.$shopping->code, ['positionClass' => 'toast-bottom-right',]);
        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $shopping = Shopping::findOrFail($id);
        return view('shopping.modal_delete', compact('shopping'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     
        $shopping = Shopping::findOrFail($id);

        if($shopping->state == '1'){
            toastr()->error('No se puede eliminar la compra debido a que no se encuentra pendiente.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }

        if($shopping->state == '1'){
            toastr()->error('No se puede eliminar la compra debido a que se encuentra validada.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }else{
            toastr()->success('Eliminada con éxito.','Compra '.$shopping->code, ['positionClass' => 'toast-bottom-right',]);
            ShoppingDetail::where('shopping_id',$id)->delete();
            $shopping->delete();
            return back();
        }
     
    }
}
