<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use App\Models\Proveedore;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\Shopping;
use App\Models\ShoppingDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::check('products.admin')) {
            $sales = Sale::orderBy('code','asc')->get();
        } else {
            $sales = Sale::orderBy('code','asc')->where('user_id',userId())->get();
        }
        return view('sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::where('state','1')->get();
        return view('sales.create', compact('clients'));
    
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'description.required'  => 'El campo descripción es obligatorio.',
            'description.max'  => 'El campo descripción no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'cliente' => ['required', Rule::exists('clients','id')],
            'description' => ['required','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $maximo=Sale::select('code')->max('code');
        $code = generateCode($maximo,'V000001','V',1,6);

        $sale = Sale::create([
            'code' => $code,
            'client_id' => $request->cliente,
            'description' => $request->description,
            'state' => '0',
            'user_id' => Auth::user()->id,
        ]);

        toastr()->success('Creada con éxito.','Cabecera de Venta', ['positionClass' => 'toast-bottom-right',]);
        return Redirect()->route('sales.show',$sale->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::findOrFail($id);
        return view('sales.show', compact('sale'));
    
    }
  
       /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $clients = Client::where('state','1')->get();
        $sale = Sale::findOrFail($id);
        return view('sales.modal_update', compact('sale','clients'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'description.required'  => 'El campo más descripción es obligatorio.',
            'description.max'  => 'El campo más descripción no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'cliente' => ['required', Rule::exists('clients','id')],
            'description' => ['required','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $sale = Sale::findOrFail($id);

        if($sale->state != '0'){
            toastr()->error('No se puede modificar los datos de la compra debido a que no se encuentra pendiente.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }

        $sale->client_id =  $request->cliente;
        $sale->description =  $request->description;
        $sale->update();

        toastr()->success('Modificada con éxito.','Venta '.$sale->code, ['positionClass' => 'toast-bottom-right']);
        return Response::json(['success' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalState($id)
    {
        $sale = Sale::findOrFail($id);
        return view('sales.modal_state', compact('sale'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState(Request $request, $id)
    {
        $validateArray = [
            'estado' => ['required', 'regex:~(^[12]{1})$~'],
        ];
        $request->validate($validateArray);

        $sale = Sale::findOrFail($id);

        if($sale->state != '0'){
            toastr()->error('No se puede cambiar de estado la venta debido a que no se encuentra pendiente.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }


        if ($request->estado == '1') {
            // VALIDACIÓN DEL STOCK
            $detailsProducts = SaleDetail::selectRaw('SUM(quantity) as canttotal, product_id')->groupBy('product_id')->get();
            foreach ($detailsProducts as $key => $detpro) {
                $product = Product::findOrFail($detpro->product_id);
                if($detpro->canttotal > $product->stock){
                    toastr()->error('No se puede validar la venta debido a que no hay stock suficiente para el producto: <b>'.$product->name.'.</b><br>Por favor modifique los datos del proceso de venta.','Stock Insuficiente', ['positionClass' => 'toast-bottom-right',]);
                    return back();
                }
            }


            $sale->state = $request->estado;
            $sale->update();

            // ACTUALIZACION DEL STOCK
            foreach ($sale->details as $key => $detail) {
                $product = $detail->product;
                $product->stock -= $detail->quantity;
                $product->update();
            }
            $sale->date_validation = now();
            $sale->update();
            
            toastr()->success('Validada con éxito.','Venta '.$sale->code, ['positionClass' => 'toast-bottom-right',]);
            toastr()->success('Actualizado exitosamente','Stock de Productos', ['positionClass' => 'toast-bottom-right',]);
        } else {
            $sale->state = $request->estado;
            $sale->update();
            toastr()->success('Anulada con éxito.','Venta '.$sale->code, ['positionClass' => 'toast-bottom-right',]);
        }
        
        return back();
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $sale = Sale::findOrFail($id);
        return view('sales.modal_delete', compact('sale'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     
        $sale = Sale::findOrFail($id);

        if($sale->state == '1'){
            toastr()->error('No se puede eliminar la venta debido a que se encuentra validada.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }else{
            toastr()->success('Eliminada con éxito.','Venta '.$sale->code, ['positionClass' => 'toast-bottom-right',]);
            SaleDetail::where('sale_id',$id)->delete();
            $sale->delete();
            return back();
        }
     
    }
}
