<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'name.required'  => 'El campo nombre es obligatorio.',
            'name.max'  => 'El campo nombre no debe contener más de 255 caracteres.',
            'email.required'  => 'El campo correo electrónico es obligatorio.',
            'email.email'  => 'El campo  correo electrónico debe ser una dirección de correo válida.',
            'email.max'  => 'El campo  correo electrónico no debe contener más de 255 caracteres.',
            'password.required'  => 'El campo contraseña es obligatorio.',
            'password.min'  => 'El campo contraseña debe tener al menos 8 caracteres.',
            'password.confirmed'  => 'Los campos de contraseña no son iguales.',
            'last_name.required'  => 'El campo apellido es obligatorio.',
            'last_name.max'  => 'El campo apellido no debe contener más de 255 caracteres.',
            'last_name.min'  => 'El campo apellido debe tener al menos 2 caracteres.',
            'phone.required'  => 'El campo teléfono es obligatorio.',
            'phone.max'  => 'El campo teléfono no debe contener más de 8 dígitos.',
            'phone.min'  => 'El campo teléfono debe tener al menos 7 dígitos.',
            'phone.integer'  => 'El campo teléfono debe ser numérico',
            'city.required'  => 'El campo ciudad es obligatorio.',
            'city.max'  => 'El campo ciudad no debe contener más de 255 caracteres.',
            'city.min'  => 'El campo ciudad debe tener al menos 2 caracteres.',
            'address.required'  => 'El campo dirección es obligatorio.',
            'address.max'  => 'El campo dirección no debe contener más de 255 caracteres.',
            'address.min'  => 'El campo dirección debe tener al menos 5 caracteres.',

            'role.required'  => 'El campo rol es obligatorio.', 
        ];
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email:filter', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'last_name' => ['required', 'string', 'min:2', 'max:255'],
            'phone' => ['required', 'integer', 'min:1000000', 'max:99999999'],
            'city' => ['required', 'string', 'min:2', 'max:255'],
            'address' => ['required', 'string', 'min:5', 'max:255'],
            'role' => ['required']
        ],$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $role = Role::find($data['role']);
        $role = (isset($role->id))? $role :  Role::find(1);
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'address' => $data['address'],
            'state' => '1',
        ])->assignRole($role);
    }
}
