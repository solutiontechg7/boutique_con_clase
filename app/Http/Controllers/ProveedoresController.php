<?php

namespace App\Http\Controllers;

use App\Models\Proveedore;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Proveedore::orderBy('name','asc')->get();
        // Session::put('item', '13.');
        return view('Providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required'  => 'El campo nombre ó razón social es obligatorio.',
            'name.max'  => 'El campo nombre ó razón social no debe contener más de 255 caracteres.',
            'name.unique'  => 'El campo nombre ó razón social ya se encuentra registrado.',
            'email.email'  => 'El campo  correo electrónico debe ser una dirección de correo válida.',
            'email.max'  => 'El campo  correo electrónico no debe contener más de 255 caracteres.',
            'phone.required'  => 'El campo teléfono es obligatorio.',
            'phone.max'  => 'El campo teléfono no debe contener más de 8 dígitos.',
            'phone.min'  => 'El campo teléfono debe tener al menos 7 dígitos.',
            'phone.integer'  => 'El campo teléfono debe ser numérico',
            'nroidentification.required'  => 'El campo nro de identificación es obligatorio.',
            'nroidentification.max'  => 'El campo nro de identificación no debe contener más de 12 dígitos.',
            'nroidentification.min'  => 'El campo nro de identificación debe tener al menos 6 dígitos.',
            'nroidentification.integer'  => 'El campo nro de identificación debe ser numérico',
            'address.required'  => 'El campo dirección es obligatorio.',
            'address.max'  => 'El campo dirección no debe contener más de 200 caracteres.',
            'address.min'  => 'El campo dirección debe tener al menos 5 caracteres.',
            'information.max'  => 'El campo más información no debe contener más de 300 caracteres.',
        ];
        $validateArray = [
            'name' => ['required', 'string', 'max:200', Rule::unique('proveedores', 'name')],
            'email' => ['nullable', 'string', 'email:filter', 'max:200'],
            'phone' => ['required', 'integer', 'min:1000000', 'max:99999999'],
            'nroidentification' => ['nullable', 'integer', 'min:100000', 'max:999999999999'],
            'address' => ['required', 'string', 'min:5', 'max:200'],
            'information' => ['nullable','max:300'],
        ];
        $request->validate($validateArray, $messages);


        $provider = Proveedore::create([
            'name' => $request->name,
            'nro_identification' => $request->nroidentification,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'information' => $request->information,
            'state' => '1',
            'user_id' => Auth::user()->id,
        ]);

        toastr()->success('Creado con éxito.','Proveedor '.$provider->name, ['positionClass' => 'toast-bottom-right',]);
        return Redirect()->route('providers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalState($id)
    {
        $provider = Proveedore::findOrFail($id);
        return view('Providers.modal_state', compact('provider'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState($id)
    {
        $provider = Proveedore::findOrFail($id);
        $provider->state = ($provider->state == '1')? '2' : '1';
        $provider->update();
        if ($provider->state == '1') {
            toastr()->success('Activado con éxito.','Proveedor '.$provider->name, ['positionClass' => 'toast-bottom-right',]);
        } else {
            toastr()->success('Inactivado con éxito.','Proveedor '.$provider->name, ['positionClass' => 'toast-bottom-right',]);
        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $provider = Proveedore::findOrFail($id);
        return view('Providers.modal_update', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required'  => 'El campo nombre ó razón social es obligatorio.',
            'name.max'  => 'El campo nombre ó razón social no debe contener más de 255 caracteres.',
            'name.unique'  => 'El campo nombre ó razón social ya se encuentra registrado.',
            'email.email'  => 'El campo  correo electrónico debe ser una dirección de correo válida.',
            'email.max'  => 'El campo  correo electrónico no debe contener más de 255 caracteres.',
            'phone.required'  => 'El campo teléfono es obligatorio.',
            'phone.max'  => 'El campo teléfono no debe contener más de 8 dígitos.',
            'phone.min'  => 'El campo teléfono debe tener al menos 7 dígitos.',
            'phone.integer'  => 'El campo teléfono debe ser numérico',
            'nroidentification.required'  => 'El campo nro de identificación es obligatorio.',
            'nroidentification.max'  => 'El campo nro de identificación no debe contener más de 12 dígitos.',
            'nroidentification.min'  => 'El campo nro de identificación debe tener al menos 6 dígitos.',
            'nroidentification.integer'  => 'El campo nro de identificación debe ser numérico',
            'address.required'  => 'El campo dirección es obligatorio.',
            'address.max'  => 'El campo dirección no debe contener más de 200 caracteres.',
            'address.min'  => 'El campo dirección debe tener al menos 5 caracteres.',
            'information.max'  => 'El campo más información no debe contener más de 300 caracteres.',
        ];

        $validateArray = [
            'name' => ['required', 'string', 'max:200', Rule::unique('proveedores', 'name')->ignore($id)],
            'email' => ['nullable', 'string', 'email:filter', 'max:200'],
            'phone' => ['required', 'integer', 'min:1000000', 'max:99999999'],
            'nroidentification' => ['nullable', 'integer', 'min:100000', 'max:999999999999'],
            'address' => ['required', 'string', 'min:5', 'max:200'],
            'information' => ['nullable','max:300'],
        ];
        $request->validate($validateArray, $messages);

        $provider = Proveedore::findOrFail($id);
        $provider->name =  $request->name;
        $provider->nro_identification =  $request->nroidentification;
        $provider->phone =  $request->phone;
        $provider->email =  $request->email;
        $provider->address =  $request->address;
        $provider->information =  $request->information;
        $provider->update();

        toastr()->success('Modificado con éxito.','Proveedor '.$provider->name, ['positionClass' => 'toast-bottom-right',]);
        return Response::json(['success' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $provider = Proveedore::findOrFail($id);
        return view('Providers.modal_delete', compact('provider'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provider = Proveedore::findOrFail($id);
        $provider->delete();
        toastr()->success('Eliminado con éxito.','Proveedor '.$provider->name, ['positionClass' => 'toast-bottom-right',]);
        return back();
    }
}
