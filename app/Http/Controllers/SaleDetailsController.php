<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\Shopping;
use App\Models\ShoppingDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class SaleDetailsController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalCreate($id)
    {
        $sale = Sale::findOrFail($id);
        $products = Product::where('state','1')->where('stock','>',0)->get();
        return view('sales.details.modal_create', compact('sale','products'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request['cantidad'] = monedaVal($request->cantidad);
        $request['precioUnitario'] = monedaVal($request->precioUnitario);
        $request['descuento'] = monedaVal($request->descuento);
        
        $product = Product::find($request->producto);
        $cantidadMaxima = (isset($product->stock))? $product->stock :  9999999999;
        $validateArray = [
            'producto' => ['required', Rule::exists('products','id')],
            'cantidad' => ['required', 'integer', 'min:1','max:'.$cantidadMaxima],
            'precioUnitario' => ['required', 'numeric', 'min:0.1','max:9999999999.99'],
            'descuento' => ['nullable', 'numeric', 'min:0','max:'.(($request->precioUnitario*$request->cantidad)-0.01)],
            'observacion' => ['nullable', 'string', 'max:200'],
        ];
        $request->validate($validateArray);

        $sale = Sale::findOrFail($id);

        $saleDetail = SaleDetail::create([
            'product_id' => $request->producto,
            'quantity' => $request->cantidad,
            'pu' => $request->precioUnitario,
            'discount' => $request->descuento,
            'observation' => $request->observacion,
            'user_id' => Auth::user()->id,
            'sale_id' => $sale->id
        ]);

        toastr()->success('Agregado con éxito.','Detalle de Venta', ['positionClass' => 'toast-bottom-right',]);
        return Response::json(['success' => '1']);
    }

           /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalUpdate($id)
    {
        $products = Product::where('state','1')->where('stock','>',0)->get();
        $detail = SaleDetail::findOrFail($id);
        return view('sales.details.modal_update', compact('detail','products'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['cantidad'] = monedaVal($request->cantidad);
        $request['precioUnitario'] = monedaVal($request->precioUnitario);
        $request['descuento'] = monedaVal($request->descuento);
     
        $product = Product::find($request->producto);
        $cantidadMaxima = (isset($product->stock))? $product->stock :  9999999999;

        $validateArray = [
            'producto' => ['required', Rule::exists('products','id')],
            'cantidad' => ['required', 'integer', 'min:1','max:'.$cantidadMaxima],
            'precioUnitario' => ['required', 'numeric', 'min:0.1','max:9999999999.99'],
            'descuento' => ['nullable', 'numeric', 'min:0','max:'.(($request->precioUnitario*$request->cantidad)-0.01)],
            'observacion' => ['nullable', 'string', 'max:200'],
        ];
        $request->validate($validateArray);

        $detail = SaleDetail::findOrFail($id);
        $detail->product_id =  $request->producto;
        $detail->quantity =  $request->cantidad;
        $detail->pu =  $request->precioUnitario;
        $detail->discount =  $request->descuento;
        $detail->observation =  $request->observacion;
        $detail->update();

        toastr()->success('Modificado con éxito.','Detalle de Venta', ['positionClass' => 'toast-bottom-right']);
        return Response::json(['success' => '1']);
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modalDestroy($id)
    {
        $detail = SaleDetail::findOrFail($id);
        return view('sales.details.modal_delete', compact('detail'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     
        $detail = SaleDetail::findOrFail($id);

        if($detail->sale->state == '1'){
            toastr()->error('No se puede eliminar el detalle de la venta debido a que la venta se encuentra validada.','', ['positionClass' => 'toast-bottom-right',]);
            return back();
        }else{
            toastr()->success('Eliminado con éxito.','Detalle de Venta', ['positionClass' => 'toast-bottom-right',]);
            $detail->delete();
            return back();
        }
     
    }
    
}
