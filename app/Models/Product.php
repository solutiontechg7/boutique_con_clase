<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'pu_shopping',
        'pu_sale',
        'stock',
        'state',
        'marca',
        'talla',
        'color',
        'user_id',
    ];

    // RELACION A USUARIOS
    public function userCreate() {
        return $this->belongsTo(User::class,'user_id');
    }

    // ACCESSOR QUE DEVUELVE EL NOMBRE DEL ESTADO Y LA ACCION A CAMBIAR
    public function getNameStateAttribute()
    {
        return ($this->state == '1')? ['Activo','inactivar'] : ['Inactivo','activar'] ;
    }

    // RELACION A VENTAS
    public function saleDetails() {
        return $this->hasMany(SaleDetail::class,'product_id');
    }

    // RELACION A COMPRAS
    public function shoppingDetails() {
        return $this->hasMany(ShoppingDetail::class,'product_id');
    }
}
