<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedore extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'nro_identification',
        'email',
        'phone',
        'address',
        'information',
        'state',
        'user_id',
    ];

    public function userCreate() {
        return $this->belongsTo(User::class,'user_id');
    }

    public function getNameStateAttribute()
    {
        return ($this->state == '1')? ['Activo','inactivar'] : ['Inactivo','activar'] ;
    }

    // RELACION A COMPRAS
    public function shoppings() {
        return $this->hasMany(Shopping::class,'proveedor_id');
    }
}
