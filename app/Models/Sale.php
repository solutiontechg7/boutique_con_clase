<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'description',
        'client_id',
        'user_id',
        'state',
        'date_validation',
    ];

    // RELACION A USUARIOS
    public function userCreate() {
        return $this->belongsTo(User::class,'user_id');
    }

    // RELACION A CLIENTES
    public function client() {
        return $this->belongsTo(Client::class,'client_id');
    }

    // RELACION A DETALLES DE COMPRA
    public function details() {
        return $this->hasMany(SaleDetail::class,'sale_id');
    }
    
    // ACCESSOR QUE DEVUELVE EL NOMBRE DEL ESTADO Y LA ACCION A CAMBIAR
    public function getNameStateAttribute()
    {
        return ($this->state == '1')? ['Activo','inactivar'] : ['Inactivo','activar'] ;
    }

    // ACCESSOR QUE DEVUELVE EL SUBTOTAL
    public function getTotalCompraAttribute()
    {
        return (count($this->details) > 0)? $this->details->sum('subtotal') : 0;
    }

    // ACCESSOR QUE DEVUELVE EL ESTADO DE LA COMPRA
    public function getState($sw)
    {
        $state1 = "Desconocido";
        switch ($this->state) {
            case '0':
                $state1 = '<span style="padding:6px;" class="name badge text-warning">Pendiente</span>';
                $state2 = '<span style="padding:4px;" class="badge badge-warning">Pendiente</span>';
                break;
            case '1':
                $state1 = '<span style="padding:6px;" class="name badge text-success">Validada</span>';
                $state2 = '<span style="padding:4px;" class="badge badge-success">Validada</span>';

                break;
            case '2':
                $state1 = '<span style="padding:6px;" class="name badge text-danger">Anulada</span>';
                $state2 = '<span style="padding:4px;" class="badge badge-danger">Anulada</span>';
            break;
        }

        switch ($sw) {
            case '1':
                return  $state1;
                break;
            case '2':
                return  $state2;
            break;
        }
     
    }
}
