<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'nro_identification',
        'email',
        'phone',
        'address',
        'information',
        'nivel',
        'state',
        'user_id',
    ];

    // RELACION A USUARIOS
    public function userCreate() {
        return $this->belongsTo(User::class,'user_id');
    }

    // ACCESSOR QUE DEVUELVE EL NOMBRE DEL ESTADO Y LA ACCION A CAMBIAR
    public function getNameStateAttribute()
    {
        return ($this->state == '1')? ['Activo','inactivar'] : ['Inactivo','activar'] ;
    }

    // RELACION A VENTAS
    public function sales() {
        return $this->hasMany(Sale::class,'client_id');
    }
}
