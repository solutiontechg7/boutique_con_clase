<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'quantity',
        'pu',
        'discount',
        'observation',
        'shopping_id',
        'product_id',
        'user_id',
    ];

    // RELACION A USUARIOS
    public function userCreate() {
        return $this->belongsTo(User::class,'user_id');
    }

    // RELACION A LA CABECERA DE VENTAS
    public function shopping() {
        return $this->belongsTo(Shopping::class,'shopping_id');
    }
    
    // RELACION A PRODUCTOS
    public function product() {
        return $this->belongsTo(Product::class,'product_id');
    }

    // ACCESSOR QUE DEVUELVE EL SUBTOTAL
    public function getSubtotalAttribute()
    {
        return ($this->quantity*$this->pu)-$this->discount;
    }
}
