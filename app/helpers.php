<?php

use App\Models\User;

/**
  * Devuelve el número de id del usuario conectado
  * @return int $id
  */
  function userId(){
    return auth()->user()->id;
  }


  /**
 * Devuelve el codigo generado
 * @param $maximo El registro máximo Ejm: OV000234
 * @param $code Es el inicial Ejm: OV000001
 * @param $letter La letra(s) que corresponden al código Ejm: OV
 * @param $cant_letras La cantidad de letras que debe tener el código Ejm: 2
 * @param $zero La cantidad de digitos (en muchos casos ceros) con la cual se debe completar el código Ejm: 6
 * @return $cod Codigo final Ejm: OV000235
 */
function generateCode($maximo,$code,$letter,$cant_letras,$zero){
    if($maximo==null)
    {
        $cod=$code;
    }else{
        $cont=substr($maximo, $cant_letras);
        $cont=$cont+1;
        $codConCeros=str_pad($cont, $zero,"0", STR_PAD_LEFT);//relleno con cinco ceros
        $cod=$letter.$codConCeros;
    }
    return $cod;
  }

  /**
 * Devuelve el nombre y el apellido paterno de un usuario
 * @param  int $id ID del usuario del cual que quiere obtener su nombre
 * @return string $user_name
 */
 function userFullName($id){
  if($id!=null){
    $user=User::select('name','last_name')->whereId($id)->first();
    return ($user!=null) ? $user->name." ".$user->last_name : "Usuario Desconocido";
  }else{
    return "No Tiene Usuario Asignado";
  }
}

/**
 * Devuelve un valor numerico dado un valor con formato de moneda
 * @param string $valor Valor con formato de moneda Ejm; 2,020,243.45
 * @return float $numero Numero de tipo float Ejm: 2020243.45
 */
  function monedaVal($valor){
    $numero=floatval(str_replace(",","",$valor));
    return $numero;
  }