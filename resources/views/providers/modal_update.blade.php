<form method="POST" action="{{ route('providers.update',$provider->id) }}" id="form-update-provider">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <div class="row mb-3">
                
                <div class="col-md-6">
                    <label for="name" class="">* Nombre ó Razón Social </label>
                    <input id="name" type="text" class="form-control" name="name" value="{{$provider->name}}" autocomplete="name" autofocus>
                    <span id="name-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="nroidentification" class="">NIT ó CI</label>
                    <input id="nroidentification" type="text" class="form-control" name="nroidentification" value="{{$provider->nro_identification}}" autocomplete="nroidentification" autofocus>
                    <span id="nroidentification-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="email" class="">Correo Electrónico</label>
                    <input id="email" type="text" class="form-control" name="email" value="{{$provider->email}}" autocomplete="email">
                    <span id="email-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="phone" class="">* Teléfono</label>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{$provider->phone}}" autocomplete="phone" autofocus>
                    <span id="phone-error" class="text-danger"></span>
                </div> 
            </div>

            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="address" class="">* Dirección</label>
                    <input id="address" type="text" class="form-control" name="address" value="{{$provider->address}}" autocomplete="address" autofocus>
                    <span id="address-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="information" class="">Más Información</label>
                    <textarea name="information" class="form-control" id="information" cols="30" rows="3">{{$provider->information}}</textarea>
                    <span id="information-error" class="text-danger"></span>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Actualizar</button>
    </div>
</form>

<script>
    
    var campos = ['name','nroidentification','email','information','phone','address'];
    $("#form-update-provider").on('submit', function(e) {
        $("[name=btnSubmit]").attr('disabled',true);
        e.preventDefault();
        $('.divMensajeDeEspera').slideDown();
        var registerForm = $("#form-update-provider");
        var formData = new FormData($("#form-update-provider")[0]);
        $.each(campos, function( indice, valor ) {
            $("#"+valor+"-error").html( "" );
            $("[name="+valor+"]").removeClass('is-invalid').addClass('is-valid');
            $("select[name="+valor+"]").removeClass('is-invalid-select').addClass('is-valid-select').removeClass('select2-selection');
        });
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('providers.update',$provider->id)}}",
            type: "POST",
            data:formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("[name=btnSubmit]").attr('disabled',true)
            },
            success:function(data) {
                $('.divMensajeDeEspera').hide();
                $(".startab").hide();
                if(data.alerta) {
                    // toastr.error(data.mensaje);
                    $("[name=btnSubmit]").attr('disabled',false)
                }else if(data.success == '1') {
                    $("[name=btnSubmit]").attr('disabled',true)
                    window.location.reload();
                }else if(typeof(data.status) == "undefined"){
          
                }

            },
            error: function(data){
                $('.divMensajeDeEspera').hide();
                $("[name=btnSubmit]").attr('disabled',false);
                if(data.responseJSON.errors) {
                    var contErrors = 0;
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#form-update-provider #'+index+'-error' ).html( value );
                        $("#form-update-provider [name="+index+"]").removeClass('is-valid').addClass('is-invalid');
                        $("#form-update-provider select[name="+index+"]").removeClass('is-valid-select').addClass('is-invalid-select').removeClass('select2-selection');
                   
                        if (contErrors == 0) {
                            var divPadre1 = $("#form-update-provider [name="+index+"]").closest('div.col-xs-12')[0];
                            var divPadre2 = $("#form-update-provider [name="+index+"]").closest('div')[0];
                            if(divPadre1 != null){
                                divPadre1.scrollIntoView({behavior: 'smooth'});
                            }else if(divPadre2 != null){
                                divPadre2.scrollIntoView({behavior: 'smooth'});
                            }
                        }
                        contErrors++;
                    });
                }
                if(typeof(data.status) != "undefined" && data.status != null && data.status == '401'){
                    window.location.href = '/login';
                }
            }
        });
    });

</script>