@extends('layouts.app', ['title_template' => "Proveedores"])

@section('content_header')
    <div class="title_left">
        <h3>Proveedores</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="text-right">
                <a href="/providers/create" class="btn btn-round btn-primary"><i class="fa fa-plus"></i> Nuevo Proveedor</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"><br></div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-hover table-striped letraMin small" style="vertical-align: middle" id="table_providers">
                    <thead>
                    <tr role="row" >
                        <th width="5%" class="text-center align-middle"  style="vertical-align: middle">N°</th>
                        <th width="15%" class="text-center align-middle">NOMBRE<br>RAZÓN SOCIAL</th>
                        <th width="10%" class="text-center align-middle">NIT/CI</th>
                        <th width="12%" class="text-center align-middle">CORREO ELECTRÓNICO</th>
                        <th width="5%" class="text-center align-middle">TELÉFONO</th>
                        <th width="13%" class="text-center align-middle">DIRECCIÓN</th>
                        <th width="20%" class="text-center align-middle">MÁS INFORMACIÓN</th>
                        <th width="10%" class="text-center align-middle">REGISTRADO POR</th>
                        <th width="5%" class="text-center align-middle">ESTADO</th> 
                        <th width="5%" class="text-center align-middle"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($providers as $key => $provider)
                            <tr>
                                <td class="text-center align-middle">{{$key+1}}</td>
                                <td class="align-middle">{{$provider->name}}</td>
                                <td class="text-center align-middle">{{$provider->nro_identification}}</td> 
                                <td class="align-middle">{{$provider->email}}</td> 
                                <td class="text-center align-middle">{{$provider->phone}}</td> 
                                <td class="align-middle">{{$provider->address}}</td> 
                                <td class="align-middle"><div data-toggle="tooltip" data-placement="top" title="{{$provider->information}}">{{Str::limit($provider->information, 70, " ...")}}</div></td> 
                                <td class="text-center align-middle">{{$provider->userCreate->name.' '.$provider->userCreate->last_name}}</td> 
                                <td class="text-center align-middle"><span style="padding:3px;" class="badge badge-{{($provider->state == '1')? 'success' : 'danger'}}">{{($provider->state == '1')? 'Activo' : 'Inactivo'}}</span></td>
                                <td class="text-center align-middle">
                                    <a href="/providers/modal_state/{{$provider->id}}" rel="modalState" class="p-0" data-toggle="tooltip" data-placement="top" title="Cambiar de estado">
                                        <i class="fa fa-user fa-lg"></i>
                                    </a>
                                    <a  href="/providers/modal_update/{{$provider->id}}" rel="modalUpdate" class="p-0" data-toggle="tooltip" data-placement="top" title="Editar proveedor">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>
                                    @if (count($provider->shoppings) == 0)                                        
                                    <a href="/providers/modal_destroy/{{$provider->id}}" rel="modalDestroy" class="p-0" data-toggle="tooltip" data-placement="top" title="ELiminar proveedor">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
{{-- cambiar de estado --}}
<div class="modal fade" id="modalState" tabindex="-1" role="dialog" aria-labelledby="modalState" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- editar usuario --}}
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- eliminar usuario --}}
<div class="modal fade" id="modalDestroy" tabindex="-1" role="dialog" aria-labelledby="modalDestroy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
// Agregando datatable
$(document).ready(function () {

    // DataTable
    var table = $('#table_providers').DataTable({
        // 'order': [[ 1, "asc" ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'mark': true,
        'autoWidth': false,
        "pageLength": 25,
        "columnDefs": [{
            "targets": [0, 8],
            "orderable": false
        }],
        "drawCallback": function () {
            // funcion search general
            $('.inputSearchDT').on('paste', function (e) {
                var valor = e.originalEvent.clipboardData.getData('Text');
                var id = $(this).attr('id');
                if (noPegar(valor, id, 'left') == 1) e.preventDefault();
            });
            //popover
            $(function () {
                $('[data-toggle="popover"]').popover({
                    html: true
                })
            });
        }
    });
    // Apply the search
    // table.columns().eq(0).each(function (colIdx) {
    //     $('input', $('.filters td')[colIdx]).on('keyup change', function () {
    //         table
    //             .column(colIdx)
    //             .search(this.value)
    //             .draw();
    //     });
    // });

    // input search para el excel
    // $("input[type='search']").focusout(function () {
    //     $("#searchDT").val(table.search());
    // });
});

// MODAL POR AJAX
$('a[rel=modalState]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalState').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});

$('a[rel=modalUpdate]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalUpdate').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalDestroy]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalDestroy').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


</script>

@endsection