<form method="POST" action="{{ route('providers.destroy',$provider->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <span style="font-size: 16px;">
                Proveedor: <b>{{$provider->name}}</b>
            </span>
            <br>
            <span style="font-size: 14px;">
                ¿Esta seguro de eliminar al proveedor?
            </span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
    </div>
</form>