<form method="POST" action="{{ route('products.updateState',$product->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cambiar de estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <span style="font-size: 16px;">
                Producto: <b>{{$product->name}}</b>
            </span>
            <br>
            <span style="font-size: 14px;">
                ¿Está seguro de {{$product->nameState[1]}} al producto?
            </span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-sm">Cambiar</button>
    </div>
</form>