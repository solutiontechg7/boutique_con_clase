<form method="POST" action="{{ route('products.update',$product->id) }}" id="form-update-product" autocomplete="off">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <div class="row mb-3">
                
                <div class="col-md-12">
                    <label for="name" class="">* Nombre del producto </label>
                    <input id="name" type="text" class="form-control" name="name" value="{{$product->name}}" autocomplete="name" autofocus>
                    <span id="name-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="marca" class="">* Marca</label>
                    <input id="marca" type="text" class="form-control" name="marca" value="{{$product->marca}}" autocomplete="marca" autofocus>
                    <span id="marca-error" class="text-danger"></span>
                </div> 
                <div class="col-md-6">
                    <label for="talla" class="">* Talla</label>
                    <input id="talla" type="text" class="form-control" name="talla" value="{{$product->talla}}" autocomplete="talla" autofocus>
                    <span id="talla-error" class="text-danger"></span>
                </div> 
              
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="color" class="">* Color</label>
                    <input id="color" type="text" class="form-control" name="color" value="{{$product->color}}" autocomplete="color">
                    <span id="color-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="stock" class="">* Stock</label>
                    <input id="stock" type="text" class="form-control" name="stock" value="{{$product->stock}}" autocomplete="stock" autofocus>
                    <span id="stock-error" class="text-danger"></span>
                </div> 
    
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="pushopping" class="">* Precio de Compra (Bs.)</label>
                    <input id="pushopping" type="text" class="form-control" name="pushopping" value="{{$product->pu_shopping}}" autocomplete="pushopping" autofocus>
                    <span id="pushopping-error" class="text-danger"></span>
                </div> 
                <div class="col-md-6">
                    <label for="pusale" class="">* Precio de Venta (Bs.)</label>
                    <input id="pusale" type="text" class="form-control" name="pusale" value="{{$product->pu_sale}}" autocomplete="pusale">
                    <span id="pusale-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="description" class="">Descripción</label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="3">{{$product->description}}</textarea>
                    <span id="description-error" class="text-danger"></span>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Actualizar</button>
    </div>
</form>

<script>
    
    var campos = ['name','marca','talla','color','stock','pushopping','pusale','descrpition'];
    $("#form-update-product").on('submit', function(e) {
        $("[name=btnSubmit]").attr('disabled',true);
        e.preventDefault();
        $('.divMensajeDeEspera').slideDown();
        var registerForm = $("#form-update-product");
        var formData = new FormData($("#form-update-product")[0]);
        $.each(campos, function( indice, valor ) {
            $("#"+valor+"-error").html( "" );
            $("[name="+valor+"]").removeClass('is-invalid').addClass('is-valid');
            $("select[name="+valor+"]").removeClass('is-invalid-select').addClass('is-valid-select').removeClass('select2-selection');
        });
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('products.update',$product->id)}}",
            type: "POST",
            data:formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("[name=btnSubmit]").attr('disabled',true)
            },
            success:function(data) {
                $('.divMensajeDeEspera').hide();
                $(".startab").hide();
                if(data.alerta) {
                    // toastr.error(data.mensaje);
                    $("[name=btnSubmit]").attr('disabled',false)
                }else if(data.success == '1') {
                    $("[name=btnSubmit]").attr('disabled',true)
                    window.location.reload();
                }else if(typeof(data.status) == "undefined"){
          
                }

            },
            error: function(data){
                $('.divMensajeDeEspera').hide();
                $("[name=btnSubmit]").attr('disabled',false);
                if(data.responseJSON.errors) {
                    var contErrors = 0;
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#form-update-product #'+index+'-error' ).html( value );
                        $("#form-update-product [name="+index+"]").removeClass('is-valid').addClass('is-invalid');
                        $("#form-update-product select[name="+index+"]").removeClass('is-valid-select').addClass('is-invalid-select').removeClass('select2-selection');
                   
                        if (contErrors == 0) {
                            var divPadre1 = $("#form-update-product [name="+index+"]").closest('div.col-xs-12')[0];
                            var divPadre2 = $("#form-update-product [name="+index+"]").closest('div')[0];
                            if(divPadre1 != null){
                                divPadre1.scrollIntoView({behavior: 'smooth'});
                            }else if(divPadre2 != null){
                                divPadre2.scrollIntoView({behavior: 'smooth'});
                            }
                        }
                        contErrors++;
                    });
                }
                if(typeof(data.status) != "undefined" && data.status != null && data.status == '401'){
                    window.location.href = '/login';
                }
            }
        });
    });

    // AutoNumeric
    $(document).ready(function () {
        new AutoNumeric('#pushopping', {
            minimumValue: 0,
            decimalPlaces: 2
        });
        new AutoNumeric('#pusale', {
            minimumValue: 0,
            decimalPlaces: 2
        });
        new AutoNumeric('#stock', {
            minimumValue: 0,
            decimalPlaces: 0
        });
    });
</script>