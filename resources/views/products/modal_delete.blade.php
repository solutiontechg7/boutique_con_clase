<form method="POST" action="{{ route('products.destroy',$product->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <span style="font-size: 16px;">
                Producto: <b>{{$product->name}}</b>
            </span>
            <br>
            <span style="font-size: 14px;">
                ¿Está seguro de eliminar el producto?
            </span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
    </div>
</form>