@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><i class="fa fa-cubes"></i> Registrar Nuevo Producto</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('products.store') }}" autocomplete="off">
                        @csrf

                        <div class="row mb-3">
                            
                            <div class="col-md-12">
                                <label for="name" class="">* Nombre del producto</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="marca" class="">* Marca</label>
                                <input id="marca" type="text" class="form-control @error('marca') is-invalid @enderror" name="marca" value="{{ old('marca') }}" autofocus>
                                @error('marca')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label for="talla" class="">* Talla</label>
                                <input id="talla" type="text" class="form-control @error('talla') is-invalid @enderror" name="talla" value="{{ old('talla') }}"  autofocus>
                                @error('talla')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label for="color" class="">* Color</label>
                                <input id="color" type="text" class="form-control @error('color') is-invalid @enderror" name="color" value="{{ old('color') }}" autofocus>
                                @error('color')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">

                            <div class="col-md-4">
                                <label for="stock" class="">* Stock Inicial </label>
                                <input id="stock" type="text" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}" autofocus>
                                @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label for="pushopping" class="">* Precio de Compra (Bs.)</label>
                                <input id="pushopping" type="text" class="form-control @error('pushopping') is-invalid @enderror" name="pushopping" value="{{ old('pushopping') }}" autofocus>
                                @error('pushopping')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label for="pusale" class="">* Precio de Venta (Bs.)</label>
                                <input id="pusale" type="text" class="form-control @error('pusale') is-invalid @enderror" name="pusale" value="{{ old('pusale') }}" autofocus>
                                @error('pusale')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label for="description" class="">Descripción</label>
                                <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="3" autofocus>{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
        // AutoNumeric
    $(document).ready(function () {
    new AutoNumeric('#pushopping', {
        minimumValue: 0,
        decimalPlaces: 2
    });
    new AutoNumeric('#pusale', {
        minimumValue: 0,
        decimalPlaces: 2
    });
    new AutoNumeric('#stock', {
        minimumValue: 0,
        decimalPlaces: 0
    });
    });
</script>
 @endsection