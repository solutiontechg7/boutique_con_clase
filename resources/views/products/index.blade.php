@extends('layouts.app', ['title_template' => "Productos"])

@section('content_header')
    <div class="title_left">
        <h3>Productos</h3>
    </div>

    @if (Gate::check('products.admin')) 
    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="text-right">
                <a href="/products/create" class="btn btn-round btn-primary"><i class="fa fa-plus"></i> Nuevo Producto</a>
            </div>
        </div>
    </div>
    @endif
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"><br></div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-hover table-striped letraMin small" style="vertical-align: middle" id="table_products">
                    <thead>
                    <tr role="row" >
                        <th width="5%" class="text-center align-middle"  style="vertical-align: middle">CODIGO</th>
                        <th width="15%" class="text-center align-middle">NOMBRE</th>
                        <th width="5%" class="text-center align-middle">STOCK</th>
                        <th width="10%" class="text-center align-middle">MARCA</th>
                        <th width="10%" class="text-center align-middle">TALLA</th>
                        <th width="10%" class="text-center align-middle">COLOR</th>
                        <th width="12%" class="text-center align-middle">DESCRIPCIÓN</th>
                        @if (Gate::check('products.admin'))                            
                        <th width="8%" class="text-center align-middle">P.U. DE COMPRA</th>
                        @endif
                        <th width="8%" class="text-center align-middle">P.U. DE VENTA</th>
                        <th width="10%" class="text-center align-middle">REGISTRADO POR</th>
                        <th width="5%" class="text-center align-middle">ESTADO</th> 
                        @if (Gate::check('products.admin'))                            
                        <th width="2%" class="text-center align-middle"></th>
                        @endif

                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                        <tr>
                            <td class="text-center align-middle">{{$product->code}}</td>
                            <td class="align-middle">{{$product->name}}</td>
                            <td class="text-center align-middle">{{$product->stock}}</td> 
                            <td class="align-middle">{{$product->marca}}</td> 
                            <td class="text-center align-middle">{{$product->talla}}</td> 
                            <td class="text-center align-middle">{{$product->color}}</td> 
                            <td class="align-middle">{{$product->description}}</td> 
                            <td class="text-center align-middle"><span class="text-success">Bs.</span> {{number_format($product->pu_shopping,2)}}</td> 
                            @if (Gate::check('products.admin'))                            
                            <td class="text-center align-middle"><span class="text-success">Bs.</span> {{number_format($product->pu_sale,2)}}</td> 
                            @endif
                            <td class="text-center align-middle">{{$product->userCreate->name.' '.$product->userCreate->last_name}}</td> 
                            <td class="text-center align-middle"><span style="padding:3px;" class="badge badge-{{($product->state == '1')? 'success' : 'danger'}}">{{($product->state == '1')? 'Activo' : 'Inactivo'}}</span></td>
                            @if (Gate::check('products.admin'))                            
                            <td class="text-center align-middle">
                                @can('products.admin')
                                    <a href="/products/modal_state/{{$product->id}}" rel="modalState" class="p-0" data-toggle="tooltip" data-placement="top" title="Cambiar de estado"><i class="fa fa-user fa-lg"></i></a>
                                    <a  href="/products/modal_update/{{$product->id}}" rel="modalUpdate" class="p-0" data-toggle="tooltip" data-placement="top" title="Editar producto">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>
                                    @if (count($product->shoppingDetails) == 0 && count($product->saleDetails) == 0)
                                    <a href="/products/modal_destroy/{{$product->id}}" rel="modalDestroy" class="p-0" data-toggle="tooltip" data-placement="top" title="ELiminar producto"><i class="fa fa-trash-o fa-lg"></i></a>
                                    @endif

                                @endcan
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
{{-- cambiar de estado --}}
<div class="modal fade" id="modalState" tabindex="-1" role="dialog" aria-labelledby="modalState" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- editar usuario --}}
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- eliminar usuario --}}
<div class="modal fade" id="modalDestroy" tabindex="-1" role="dialog" aria-labelledby="modalDestroy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
// Agregando datatable
$(document).ready(function () {

    // DataTable
    var table = $('#table_products').DataTable({
        // 'order': [[ 1, "asc" ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'mark': true,
        'autoWidth': false,
        "pageLength": 25,
        "columnDefs": [{
            "targets": [0, 8],
            "orderable": false
        }],
        "drawCallback": function () {
            // funcion search general
            $('.inputSearchDT').on('paste', function (e) {
                var valor = e.originalEvent.clipboardData.getData('Text');
                var id = $(this).attr('id');
                if (noPegar(valor, id, 'left') == 1) e.preventDefault();
            });
            //popover
            $(function () {
                $('[data-toggle="popover"]').popover({
                    html: true
                })
            });
        }
    });
});

// MODAL POR AJAX
$('a[rel=modalState]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalState').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});

$('a[rel=modalUpdate]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalUpdate').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalDestroy]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalDestroy').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


</script>

@endsection