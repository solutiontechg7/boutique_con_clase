<form method="POST" action="{{ route('users.update',$user->id) }}" id="form-update-user">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <div class="row mb-3">
                
                <div class="col-md-6">
                    <label for="name" class="">* {{ __('Nombres') }}</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" autocomplete="name" autofocus>
                    <span id="name-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="last_name" class="">* Apellidos</label>
                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{$user->last_name}}" autocomplete="last_name" autofocus>
                    <span id="last_name-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="email" class="">* {{ __('Correo Electrónico') }}</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" autocomplete="email">
                    <span id="email-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="role" class="">* {{ __('Rol') }}</label>
                    <select name="role" id="role" class="form-control" placeholder="Seleccione una opción">
                        <option value="">Seleccione una opción</option>
                        @foreach ($roles as $role)
                            <option value="{{$role->id}}" {{($role->name == $user->hasRole(($role->name)))? 'selected' : ''}}>{{($role->name == 'Admin')? 'Administrador' : (($role->name == 'Ventas')? 'Personal de Ventas' : $role->name )}}</option>
                        @endforeach
                    </select>
                    <span id="role-error" class="text-danger"></span>
                </div>
            </div>

            @if (Auth::user()->id == $user->id)                
                {{-- <div class="row mb-3">
                    <div class="col-md-6">
                        <label for="password" class="">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control" name="password" autocomplete="new-password">
                    </div>
                    <div class="col-md-6">
                        <label for="password-confirm" class="">{{ __('Confirmar Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                    </div>
                </div> --}}
            @endif



            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="phone" class="">* Teléfono</label>
                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$user->phone}}" autocomplete="phone" autofocus>
                    <span id="phone-error" class="text-danger"></span>
                </div>
                <div class="col-md-6">
                    <label for="city" class="">* Ciudad</label>
                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{$user->city}}" autocomplete="city" autofocus>
                    <span id="city-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="address" class="">* Dirección</label>
                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{$user->address}}" autocomplete="address" autofocus>
                    <span id="address-error" class="text-danger"></span>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Actualizar</button>
    </div>
</form>

<script>
    
    var campos = ['name','last_name','email','role','phone','city','address'];
    $("#form-update-user").on('submit', function(e) {
        $("[name=btnSubmit]").attr('disabled',true);
        e.preventDefault();
        $('.divMensajeDeEspera').slideDown();
        var registerForm = $("#form-update-user");
        var formData = new FormData($("#form-update-user")[0]);
        $.each(campos, function( indice, valor ) {
            $("#"+valor+"-error").html( "" );
            $("[name="+valor+"]").removeClass('is-invalid').addClass('is-valid');
            $("select[name="+valor+"]").removeClass('is-invalid-select').addClass('is-valid-select').removeClass('select2-selection');
        });
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('users.update',$user->id)}}",
            type: "POST",
            data:formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("[name=btnSubmit]").attr('disabled',true)
            },
            success:function(data) {
                $('.divMensajeDeEspera').hide();
                $(".startab").hide();
                if(data.alerta) {
                    // toastr.error(data.mensaje);
                    $("[name=btnSubmit]").attr('disabled',false)
                }else if(data.success == '1') {
                    $("[name=btnSubmit]").attr('disabled',true)
                    window.location.reload();
                }else if(typeof(data.status) == "undefined"){
          
                }

            },
            error: function(data){
                $('.divMensajeDeEspera').hide();
                $("[name=btnSubmit]").attr('disabled',false);
                if(data.responseJSON.errors) {
                    var contErrors = 0;
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#form-update-user #'+index+'-error' ).html( value );
                        $("#form-update-user [name="+index+"]").removeClass('is-valid').addClass('is-invalid');
                        $("#form-update-user select[name="+index+"]").removeClass('is-valid-select').addClass('is-invalid-select').removeClass('select2-selection');
                   
                        if (contErrors == 0) {
                            var divPadre1 = $("#form-update-user [name="+index+"]").closest('div.col-xs-12')[0];
                            var divPadre2 = $("#form-update-user [name="+index+"]").closest('div')[0];
                            if(divPadre1 != null){
                                divPadre1.scrollIntoView({behavior: 'smooth'});
                            }else if(divPadre2 != null){
                                divPadre2.scrollIntoView({behavior: 'smooth'});
                            }
                        }
                        contErrors++;
                    });
                }
                if(typeof(data.status) != "undefined" && data.status != null && data.status == '401'){
                    window.location.href = '/login';
                }
            }
        });
    });

</script>