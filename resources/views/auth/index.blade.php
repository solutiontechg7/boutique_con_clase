@extends('layouts.app', ['title_template' => "Usuarios"])

@section('content_header')
    <div class="title_left">
        <h3>Usuarios</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
        <div class="text-right">
            <a href="/users_create" class="btn btn-round btn-primary"><i class="fa fa-plus"></i> Nuevo Usuario</a>
        </div>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"><br></div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-hover table-striped letraMin small" id="table_users">
                    <thead>
                    <tr role="row"  >
                        <th width="5%" class="text-center">N°</th>
                        <th width="15%" class="text-center">NOMBRE Y APELLIDO</th>
                        <th width="15%" class="text-center">CORREO ELECTRÓNICO</th>
                        <th width="5%" class="text-center">TELÉFONO</th>
                        <th width="10%" class="text-center">CUIDAD</th>
                        <th width="20%" class="text-center">DIRECCIÓN</th>
                        <th width="10%" class="text-center">ROL</th>
                        <th width="5%" class="text-center">ESTADO</th> 
                        <th width="10%" class="text-center"></th>
                    </tr>
                    </thead>
                    {{-- <thead role="row">
                            <tr class="filters">
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt1" type="text" placeholder="&#xf002; Buscar" name="cod"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt2" type="text" placeholder="&#xf002; Buscar" name="emp"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt3" type="text" placeholder="&#xf002; Buscar" name="web"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt4" type="text" placeholder="&#xf002; Buscar" name="telf"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt5" type="text" placeholder="&#xf002; Buscar" name="fax"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt6" type="text" placeholder="&#xf002; Buscar" name="contacto"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt7" type="text" placeholder="&#xf002; Buscar" name="dir"/></td>
                                <td><input style="width: 100%" class="buscar nopegar" id="provExt7" type="text" placeholder="&#xf002; Buscar" name="dir"/></td>
                                <td></td>
                            </tr>
                    </thead> --}}
                    <tbody>
                        @foreach ($users as $key => $user)
                            <tr>
                                <td class="text-center">{{$key+1}}</td>
                                <td>{{$user->name}} {{$user->last_name}}</td>
                                <td>{{$user->email}}</td> 
                                <td class="text-center">{{$user->phone}}</td> 
                                <td class="text-center">{{$user->city}}</td> 
                                <td>{{$user->address}}</td> 
                                <td class="text-center">{{($user->hasRole('Admin'))? 'Administrador' : 'Personal de Ventas'}}</td> 
                                <td class="text-center"><span style="padding:3px;" class="badge badge-{{($user->state == '1')? 'success' : 'danger'}}">{{($user->state == '1')? 'Activo' : 'Inactivo'}}</span></td>
                                <td class="text-center">
                                    <a href="/users_modal_state/{{$user->id}}" rel="modalState" class="p-1" data-toggle="tooltip" data-placement="top" title="Cambiar de estado">
                                        <i class="fa fa-user fa-lg"></i>
                                    </a>
                                    <a  href="/users_modal_update/{{$user->id}}" rel="modalUpdate" class="p-1" data-toggle="tooltip" data-placement="top" title="Editar usuario">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>
                                    <a href="/users_modal_destroy/{{$user->id}}" rel="modalDestroy" class="p-1" data-toggle="tooltip" data-placement="top" title="ELiminar usuario">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
{{-- cambiar de estado --}}
<div class="modal fade" id="modalState" tabindex="-1" role="dialog" aria-labelledby="modalState" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- editar usuario --}}
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- eliminar usuario --}}
<div class="modal fade" id="modalDestroy" tabindex="-1" role="dialog" aria-labelledby="modalDestroy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
// Agregando datatable
$(document).ready(function () {

    // DataTable
    var table = $('#table_users').DataTable({
        // 'order': [[ 1, "asc" ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'mark': true,
        'autoWidth': false,
        "pageLength": 25,
        "columnDefs": [{
            "targets": [0, 8],
            "orderable": false
        }],
        "drawCallback": function () {
            // funcion search general
            $('.inputSearchDT').on('paste', function (e) {
                var valor = e.originalEvent.clipboardData.getData('Text');
                var id = $(this).attr('id');
                if (noPegar(valor, id, 'left') == 1) e.preventDefault();
            });
            //popover
            $(function () {
                $('[data-toggle="popover"]').popover({
                    html: true
                })
            });
        }
    });
    // Apply the search
    // table.columns().eq(0).each(function (colIdx) {
    //     $('input', $('.filters td')[colIdx]).on('keyup change', function () {
    //         table
    //             .column(colIdx)
    //             .search(this.value)
    //             .draw();
    //     });
    // });

    // input search para el excel
    // $("input[type='search']").focusout(function () {
    //     $("#searchDT").val(table.search());
    // });
});

// MODAL POR AJAX
$('a[rel=modalState]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalState').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});

$('a[rel=modalUpdate]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalUpdate').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalDestroy]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalDestroy').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


</script>

@endsection