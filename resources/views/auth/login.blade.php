<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->


    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <!-- Bootstrap -->
    <link href="{{asset('/bowercomponets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('/bowercomponets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('/bowercomponets/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('/bowercomponets/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('/loginPlus/login.css')}}" rel="stylesheet">


</head>

<body >
    <div id="app">
        <!-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="#">
                    {{ config('app.name', 'Laravel') }}
                </a>

            </div>
        </nav> -->

        <main class="py-4 mt-0">
            <div class="container ">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="">
                            <!-- DISEÑO -->
                            <div class="wrapper bg-white">
                                <div class="text-center">
                                    <img src="{{asset('/images/logo.jpeg')}}" class="rounded img-fluid" alt="...">

                                    <!-- D:\xampp\htdocs\proyect_boutique_con_clase\public\images\logo.jpeg -->
                                </div>
                                <div class="h2 text-center">{{ __('Ingreso al sistema') }} </div>
                                @php
                                $swmessage = 0;
                                @endphp
                                {{-- <div class="h4 text-muted text-center pt-2">Llene los campos:</div> --}}
                                <form class="pt-3" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group py-2">
                                        <div class="input-field">
                                            <!-- <i class="fa fa-cubes p-2"> -->
                                            <i class="fa fa-user p-2"></i>

                                            <!-- </i> -->
                                            <!-- <i class="fa-wallet" ></i> -->
                                            <input type="text" id="email" placeholder="{{ __('Correo Electrónico') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                                        </div>
                                        @error('email')
                                        @php
                                        $swmessage = ($message == 'Estas credenciales no coinciden con nuestros registros.')? 1 : 0;
                                        @endphp
                                        @if ($swmessage == 0)
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @endif
                                        @enderror
                                    </div>


                                    <div class="form-group py-1 pb-2">
                                        <div class="input-field">
                                            <i class="fa fa-lock p-2"></i>
                                            <input placeholder="{{ __('Contraseña') }}" id="password" type="password" class="form-control @error('password') is-invalid @enderror {{($swmessage == '1')? 'is-invalid' : ''}}" name="password" autocomplete="current-password">


                                            <button type="button" onclick="mostrarContrasena()" class="btn bg-white text-muted">
                                                <i id="iconPasw" class="fa fa-eye"></i> </button>
                                        </div>
                                        @error('password')
                                        <div class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                        @if ($swmessage == 1)
                                        <span class="text-danger " role="alert">
                                            <strong> Estas credenciales no coinciden con nuestros registros.</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <script>
                                        function mostrarContrasena() {
                                            var tipo = document.getElementById("password");
                                            if (tipo.type == "password") {
                                                tipo.type = "text";
                                                $("#iconPasw").removeClass("fa-eye");
                                                $("#iconPasw").addClass("fa-eye-slash");
                                            } else {
                                                tipo.type = "password";
                                                $("#iconPasw").removeClass("fa-eye-slash");
                                                $("#iconPasw").addClass("fa-eye");
                                            }
                                        }
                                    </script>

                                    <button class="btn btn-block text-center my-3" type="submit">{{ __('Login') }}</button>
                                    @if (Route::has('password.request'))
                                    <a class="ml-auto" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif

                            </div>


                            </form>
                        </div>

                    </div>
                </div>

            </div>
    </div>




    </form>
    </div>
    </div>
    </div>
    </div>
    </div>
    </main>
    </div>
</body>

</html>