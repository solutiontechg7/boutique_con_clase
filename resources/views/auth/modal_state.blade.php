<form method="POST" action="{{ route('users.updateState',$user->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cambiar de estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <span style="font-size: 16px;">
                <b>{{$user->name}} {{$user->last_name}}</b>
            </span>
            <br>
            <span style="font-size: 14px;">
                ¿Esta seguro de {{$user->nameState[1]}} al usuario?
            </span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-sm">Cambiar</button>
    </div>
</form>