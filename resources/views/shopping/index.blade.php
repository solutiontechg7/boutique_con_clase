@extends('layouts.app', ['title_template' => "Compras"])

@section('content_header')
    <div class="title_left">
        <h3>Compras</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="text-right">
                <a href="/shoppings/create" class="btn btn-round btn-primary"><i class="fa fa-plus"></i> Nueva Compra</a> 
            </div>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"><br></div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-hover table-striped letraMin small" style="vertical-align: middle" id="table_shoppings">
                    <thead>
                    <tr role="row" >
                        <th width="5%" class="text-center align-middle"  style="vertical-align: middle">CODIGO</th>
                        <th width="15%" class="text-center align-middle">PROVEEDOR</th>
                        <th width="25%" class="text-center align-middle">DESCRIPCIÓN</th>
                        <th width="10%" class="text-center align-middle">MONTO TOTAL</th>
                        <th width="10%" class="text-center align-middle">FECHA DE CREACIÓN</th>
                        <th width="10%" class="text-center align-middle">FECHA DE VALIDACIÓN</th>
                        <th width="10%" class="text-center align-middle">REGISTRADO POR</th>
                        <th width="10%" class="text-center align-middle">ESTADO</th> 
                        <th width="5%" class="text-center align-middle"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($shoppings as $key => $shopping)
                        <tr>
                            <td class="text-center align-middle"><a href="/shoppings/show/{{$shopping->id}}">{{$shopping->code}}</a></td>
                            <td class="align-middle">{{$shopping->proveedor->name}}</td>
                            <td class="text-center align-middle">{{$shopping->description}}</td> 
                            <td class="text-right align-middle"><span class="text-success">Bs.</span> {{number_format($shopping->totalCompra,2)}}</td> 
                            <td class="text-center align-middle">{{date('d/m/Y',strtotime($shopping->created_at))}}</td> 
                            <td class="text-center align-middle">{{($shopping->date_validation != null && $shopping->date_validation != '0000-00-00' && $shopping->state == '1')? date('d/m/Y',strtotime($shopping->date_validation)) : ''}}</td> 
                            <td class="text-center align-middle">{{$shopping->userCreate->name.' '.$shopping->userCreate->last_name}}</td> 
                            <td class="text-center align-middle">{!!$shopping->getState(2)!!}</td>
                            <td class="text-center align-middle">
                                @if ($shopping->state != '1')
                                <a href="/shoppings/modal_destroy/{{$shopping->id}}" rel="modalDestroy" class="p-0" data-toggle="tooltip" data-placement="top" title="ELiminar compra"><i class="fa fa-trash-o fa-lg"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

{{-- eliminar compra --}}
<div class="modal fade" id="modalDestroy" tabindex="-1" role="dialog" aria-labelledby="modalDestroy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
// Agregando datatable
$(document).ready(function () {

    // DataTable
    var table = $('#table_shoppings').DataTable({
        // 'order': [[ 1, "asc" ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'mark': true,
        'autoWidth': false,
        "pageLength": 25,
        "columnDefs": [{
            "targets": [0, 8],
            "orderable": false
        }],
        "drawCallback": function () {
            // funcion search general
            $('.inputSearchDT').on('paste', function (e) {
                var valor = e.originalEvent.clipboardData.getData('Text');
                var id = $(this).attr('id');
                if (noPegar(valor, id, 'left') == 1) e.preventDefault();
            });
            //popover
            $(function () {
                $('[data-toggle="popover"]').popover({
                    html: true
                })
            });
        }
    });
});

// MODAL POR AJAX
$('a[rel=modalDestroy]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalDestroy').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


</script>

@endsection