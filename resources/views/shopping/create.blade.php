@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><i class="fa fa-cubes"></i> Registrar Nueva Compra</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('shoppings.store') }}">
                        @csrf

                        <div class="row mb-3"> 
                            <div class="col-md-6">
                                <label for="user" class="">* Registrado Por</label>
                                <input id="user" type="text" class="form-control" value="{{userFullName(userId())}}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="date" class="">* Fecha de Creación</label>
                                <input id="date" type="text" class="form-control" value="{{date('d/m/Y')}}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label for="proveedor" class="">* Proveedor</label>
                                <select name="proveedor" id="proveedor" class="form-control @error('proveedor') is-invalid @enderror">
                                    <option value="">Seleccione un proveedor</option>
                                    @foreach ($proveedores as $proveedor)
                                        <option value="{{$proveedor->id}}" {{(old('proveedor') == $proveedor->id)? 'selected' : '' }}>{{$proveedor->name}}</option>
                                    @endforeach
                                </select>
                                @error('proveedor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label for="description" class="">* Descripción o Motivo de la Compra</label>
                                <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="3" autofocus>{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
