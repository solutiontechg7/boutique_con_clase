@extends('layouts.app', ['title_template' => "Compras"])

@section('content_header')
<style>
    .title_information{
        font-size: 15px;
        font-weight: 600;
    }
</style>
    <div class="title_left">
        <h3>Compra <b>{{$shopping->code}}</b></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="text-right">
                <a href="/shoppings" class=""><i class="fa fa-list"></i> Ver Listado de Compras</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Información Principal</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if ($shopping->state == '0')
                            <li> <button href="/shoppings/create" class="btn btn-info btn-sm" onclick="$('#linkModalStateCompra').click()">Cambiar de Estado</button></li>
                            <li> <button href="/shoppings/modal_update/{{$shopping->id}}" class="btn btn-dark btn-sm" onclick="$('#linkModalUpdateCompra').click()">Editar</button></li>     
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-12 col-sm-12">
                        <ul class="stats-overview">
                            <li>
                                <span class="value stitle_information" style="font"> Proveedor </span>
                                <span class="name "> {{$shopping->proveedor->name}} </span>
                            </li>
                            <li>
                                <span class="value" style="font"> Estado de la Compra </span>
                                <span class="name "> {!!$shopping->getState(1)!!} </span>
                            </li>
                            <li>
                                <span class="value stitle_information" style="font"> Registrado por </span>
                                <span class="name "> {{userFullName($shopping->user_id)}} </span>
                            </li>
                        </ul>
                        <ul class="stats-overview">
                            <li>
                                <span class="value" style="font"> Fecha de Creación </span>
                                <span class="name "> {{date('d/m/Y',strtotime($shopping->created_at) )}} </span>
                            </li>
                            <li style="width: 60% !important;">
                                <span class="value" style="font"> Descripción o Motivo </span>
                                <span class="name "> {!!($shopping->description)!!}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12"><br></div>
        <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Detalles de la Compra </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if ($shopping->state == '0')
                            <li> <button class="btn btn-dark btn-sm" onclick="$('#linkModalCreate').click()">Agregar Detalle</button></li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center align-middle">N°</th>
                                <th width="22%" class="text-center align-middle">Producto</th>
                                <th width="12%" class="text-center align-middle">Cantidad</th>
                                <th width="12%" class="text-right align-middle">Precio Unitario</th>
                                <th width="12%" class="text-right align-middle">Descuento</th>
                                <th width="12%" class="text-right align-middle">Sub Total</th>
                                <th width="30%" class="text-center align-middle">Observación</th>
                                @if ($shopping->state == '0')
                                <th width="5%" class="text-center align-middle"></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                          @foreach ($shopping->details as $key => $detail)
                              <tr>
                                <td class="text-center align-middle">{{$key+1}}</td>
                                <td class="text-left align-middle">{{$detail->product->name}}</td> 
                                <td class="text-center align-middle">{{number_format($detail->quantity,0)}}</td> 
                                <td class="text-right align-middle"><span class="text-success">Bs.</span> {{number_format($detail->pu,2)}}</td> 
                                <td class="text-right align-middle"><span class="text-success">Bs.</span> {{number_format($detail->discount,2)}}</td> 
                                <td class="text-right align-middle"><span class="text-success">Bs.</span> {{number_format($detail->subtotal,2)}}</td> 
                                <td class="text-center align-middle">{{$detail->observation}}</td> 
                                @if ($shopping->state == '0')
                                <td class="text-center align-middle">
                                    <a href="/shopping_details/modal_update/{{$detail->id}}" rel="modalUpdateDetail" class="p-0" data-toggle="tooltip" data-placement="top" title="Editar detalle">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>
                                    <a href="/shopping_details/modal_destroy/{{$detail->id}}" rel="modalDestroyDetail" class="p-0" data-toggle="tooltip" data-placement="top" title="ELiminar detalle"><i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                                @endif
                              </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-right align-middle" colspan="5"><b>TOTAL</b></td>
                                <td class="text-right align-middle"><span class="text-success">Bs.</span> <b>{{number_format($shopping->totalCompra,2)}}</b></td>
                                <td></td>
                                @if ($shopping->state == '0')
                                <td></td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<a href="/shopping_details/modal_create/{{$shopping->id}}" rel="modalCreateDetail" id="linkModalCreate" style="display:none;">create</a>
<a href="/shoppings/modal_update/{{$shopping->id}}" rel="modalUpdateCompra" id="linkModalUpdateCompra" style="display:none;">update</a>
<a href="/shoppings/modal_state/{{$shopping->id}}" rel="modalStateCompra" id="linkModalStateCompra" style="display:none;">state</a>

{{-- editar compra --}}
<div class="modal fade" id="modalUpdateCompra" tabindex="-1" role="dialog" aria-labelledby="modalUpdateCompra" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- crear detalle --}}
<div class="modal fade" id="modalCreateDetail" tabindex="-1" role="dialog" aria-labelledby="modalCreateDetail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- update detalle --}}
<div class="modal fade" id="modalUpdateDetail" tabindex="-1" role="dialog" aria-labelledby="modalUpdateDetail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- delete detalle --}}
<div class="modal fade" id="modalDestroyDetail" tabindex="-1" role="dialog" aria-labelledby="modalDestroyDetail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

{{-- change state compra --}}
<div class="modal fade" id="modalStateCompra" tabindex="-1" role="dialog" aria-labelledby="modalStateCompra" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>




@endsection

@section('scripts')

<script>

// MODAL POR AJAX
$('a[rel=modalCreateDetail]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalCreateDetail').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalUpdateCompra]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalUpdateCompra').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});

$('a[rel=modalUpdateDetail]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalUpdateDetail').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalStateCompra]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalStateCompra').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});


$('a[rel=modalDestroyDetail]').on('click', function(evt) {
    evt.preventDefault();
    var modal = $('#modalDestroyDetail').modal();
    modal
    .find('.modal-content')
    .load($(this).attr('href'), function (responseText, textStatus) {
        if ( textStatus === 'success' ||
            textStatus === 'notmodified')
        {
            modal.show();
        }
    });
});



@foreach($errors->all() as $er)
toastr.error("{{$er}}");
@endforeach



</script>

@endsection