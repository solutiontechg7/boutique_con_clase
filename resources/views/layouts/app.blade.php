<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (!empty($title_template))
    <title>{{ $title_template }}</title>
    @else
    <title>Boutique Con Clase</title>
    @endif
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->


    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <!-- Bootstrap -->
    <link href="{{asset('/bowercomponets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('/bowercomponets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('/bowercomponets/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('/bowercomponets/animate.css/animate.min.css')}}" rel="stylesheet">


    <!-- Datatables -->

    <link href="{{asset('/bowercomponets/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/bowercomponets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/bowercomponets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}"
        rel="stylesheet">
    <link href="{{asset('/bowercomponets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}"
        rel="stylesheet">
    <link href="{{asset('/bowercomponets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}"
        rel="stylesheet">

    @toastr_css



    <!-- Custom Theme Style -->
    <link href="{{asset('/build/css/custom.min.css')}}" rel="stylesheet">
</head>

<body class="nav-md">
    {{-- <div id="app"> --}}
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-bold"></i> <span>CON CLASE</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{asset('/images/icono_user.jpg?'.rand())}}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Bienvenido,</span>
                            <h2>{{ Auth::user()->name.' '.Auth::user()->last_name }}</h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li>
                                    <a href="/"><i class="fa fa-home"></i> Inicio </a>
                                </li>
                                @if (Gate::check('users.admin'))
                                <li> <a href="/users"><i class="fa fa-users"></i> Usuarios </a></li>
                                @endif
                                @if (Gate::check('products.admin') || Gate::check('clients.admin'))
                                <li> <a href="/products"><i class="fa fa-cubes"></i> Productos </a></li>
                                @endif
                                @if (Gate::check('users.admin'))
                                <li> <a href="/providers"><i class="fa fa-building"></i> Proveedores </a></li>
                                @endif
                                @if (Gate::check('users.admin'))
                                <li> <a href="/shoppings"><i class="fa fa-shopping-cart"></i> Compras </a></li>
                                @endif
                                @if (Gate::check('clients.admin') || Gate::check('clients.admin.my'))
                                <li> <a href="/clients"><i class="fa fa-building"></i> Clientes </a></li>
                                @endif
                                @if (Gate::check('sales.admin.all') || Gate::check('sales.admin.my'))
                                <li> <a href="/sales"><i class="fa fa-shopping-cart"></i> Ventas </a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" style="width: 100%;" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <nav class="nav navbar-nav">
                        <ul class=" navbar-right">
                            <li class="nav-item dropdown open" style="padding-left: 15px;">
                                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true"
                                    id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{{asset('/images/icono_user.jpg?'.rand())}}" alt="">{{ Auth::user()->name.' '.Auth::user()->last_name }}
                                </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i>
                                        Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        @yield('content_header')
                    </div>

                    <div class="clearfix"></div>

                    @yield('content')
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="">
                    Sistema web de compra y venta de productos. <a href="#">SolutionTechG7</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    {{-- <main class="py-4">
            @yield('content')
        </main> --}}
    {{-- </div> --}}

    <!-- jQuery -->
    <script src="{{asset('/bowercomponets/jquery/dist/jquery.min.js')}}"></script>


    <!-- Datatables -->
    <script src="{{asset('/bowercomponets/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('/bowercomponets/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>

    <script src="{{asset('/bowercomponets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>

    <!-- ...or, you may also directly use a CDN :-->
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>



    <!-- Bootstrap -->
    <script src="{{asset('/bowercomponets/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('/bowercomponets/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('/bowercomponets/nprogress/nprogress.js')}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('/build/js/custom.min.js')}}"></script>

    {{-- TOASTR --}}
    @toastr_js
    @toastr_render
    
    @yield('scripts')
</body>

</html>
