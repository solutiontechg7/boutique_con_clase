<form method="POST" action="{{ route('clients.destroy',$client->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="text-center">
            <span style="font-size: 16px;">
                Cliente: <b>{{$client->name}}</b>
            </span>
            <br>
            <span style="font-size: 14px;">
                ¿Esta seguro de eliminar al cliente?
            </span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
    </div>
</form>