@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><i class="fa fa-shopping-cart"></i> Registrar Nueva Venta</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('sales.store') }}">
                        @csrf

                        <div class="row mb-3"> 
                            <div class="col-md-6">
                                <label for="user" class="">* Registrado Por</label>
                                <input id="user" type="text" class="form-control" value="{{userFullName(userId())}}" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="date" class="">* Fecha de Creación</label>
                                <input id="date" type="text" class="form-control" value="{{date('d/m/Y')}}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label for="cliente" class="">* Cliente</label>
                                <select name="cliente" id="cliente" class="form-control @error('cliente') is-invalid @enderror">
                                    <option value="">Seleccione un cliente</option>
                                    @foreach ($clients as $client)
                                        <option value="{{$client->id}}" {{(old('cliente') == $client->id)? 'selected' : '' }}>{{$client->name}}</option>
                                    @endforeach
                                </select>
                                @error('cliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label for="description" class="">* Descripción o Motivo de la Venta</label>
                                <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="3" autofocus>{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
