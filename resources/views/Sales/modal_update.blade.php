<form method="POST" action="{{ route('sales.update',$sale->id) }}" id="form-update-shopping">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar Compra {{$sale->code}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <div class="row mb-3">
                
                <div class="col-md-12">
                    <label for="cliente" class="">* Cliente</label>
                    <select name="cliente" id="cliente" class="form-control">
                        <option value="">Seleccione un cliente</option>
                        @foreach ($clients as $client)
                            <option value="{{$client->id}}" {{($sale->client->id == $client->id)? 'selected' : '' }}>{{$client->name}}</option>
                        @endforeach
                    </select>
                    <span id="cliente-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="description" class="">Descripción</label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="3">{{$sale->description}}</textarea>
                    <span id="description-error" class="text-danger"></span>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Actualizar</button>
    </div>
</form>

<script>
    
    var campos = ['name','nroidentification','email','information','phone','address'];
    $("#form-update-shopping").on('submit', function(e) {
        $("[name=btnSubmit]").attr('disabled',true);
        e.preventDefault();
        $('.divMensajeDeEspera').slideDown();
        var registerForm = $("#form-update-shopping");
        var formData = new FormData($("#form-update-shopping")[0]);
        $.each(campos, function( indice, valor ) {
            $("#"+valor+"-error").html( "" );
            $("[name="+valor+"]").removeClass('is-invalid').addClass('is-valid');
            $("select[name="+valor+"]").removeClass('is-invalid-select').addClass('is-valid-select').removeClass('select2-selection');
        });
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('sales.update',$sale->id)}}",
            type: "POST",
            data:formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("[name=btnSubmit]").attr('disabled',true)
            },
            success:function(data) {
                $('.divMensajeDeEspera').hide();
                $(".startab").hide();
                if(data.alerta) {
                    // toastr.error(data.mensaje);
                    $("[name=btnSubmit]").attr('disabled',false)
                }else if(data.success == '1') {
                    $("[name=btnSubmit]").attr('disabled',true)
                    window.location.reload();
                }else if(typeof(data.status) == "undefined"){
          
                }

            },
            error: function(data){
                $('.divMensajeDeEspera').hide();
                $("[name=btnSubmit]").attr('disabled',false);
                if(data.responseJSON.errors) {
                    var contErrors = 0;
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#form-update-shopping #'+index+'-error' ).html( value );
                        $("#form-update-shopping [name="+index+"]").removeClass('is-valid').addClass('is-invalid');
                        $("#form-update-shopping select[name="+index+"]").removeClass('is-valid-select').addClass('is-invalid-select').removeClass('select2-selection');
                   
                        if (contErrors == 0) {
                            var divPadre1 = $("#form-update-shopping [name="+index+"]").closest('div.col-xs-12')[0];
                            var divPadre2 = $("#form-update-shopping [name="+index+"]").closest('div')[0];
                            if(divPadre1 != null){
                                divPadre1.scrollIntoView({behavior: 'smooth'});
                            }else if(divPadre2 != null){
                                divPadre2.scrollIntoView({behavior: 'smooth'});
                            }
                        }
                        contErrors++;
                    });
                }
                if(typeof(data.status) != "undefined" && data.status != null && data.status == '401'){
                    window.location.href = '/login';
                }
            }
        });
    });

</script>