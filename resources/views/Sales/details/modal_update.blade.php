<form method="POST" action="{{ route('sale.details.update',$detail->id) }}" id="form-update-sale-detail" autocomplete="off">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar detalle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
            <div class="row mb-3">
                
                <div class="col-md-12">
                    <label for="producto" class="">* Producto</label>
                    <select name="producto" id="producto" class="form-control">
                        <option value="">Seleccione un producto</option>
                        @foreach ($products as $product)
                            <option value="{{$product->id}}" {{($detail->product_id == $product->id)? 'selected' : '' }}>{{$product->name}}  | stock: {{$product->stock}} unidades</option>
                        @endforeach
                    </select>
                    <span id="producto-error" class="text-danger"></span>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="cantidad" class="">* Cantidad</label>
                    <input id="cantidad" type="text" class="form-control text-center" name="cantidad" value="{{$detail->quantity}}" placeholder="Cantidad">
                    <span id="cantidad-error" class="text-danger"></span>
                </div> 
                <div class="col-md-4">
                    <label for="precioUnitario" class="">* Precio Unitario (Bs.)</label>
                    <input id="precioUnitario" type="text" class="form-control text-right" name="precioUnitario" value="{{$detail->pu}}" placeholder="Precio Unitario">
                    <span id="precioUnitario-error" class="text-danger"></span>
                </div> 
                <div class="col-md-4">
                    <label for="descuento" class="">Descuento (Bs.)</label>
                    <input id="descuento" type="text" class="form-control text-right" name="descuento" autocomplete="descuento" value="{{$detail->discount}}" placeholder="Precio Unitario">
                    <span id="descuento-error" class="text-danger"></span>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12">
                    <label for="observacion" class="">Observación</label>
                    <textarea name="observacion" class="form-control" id="observacion" cols="30" rows="2">{{$detail->observation}}</textarea>
                    <span id="observacion-error" class="text-danger"></span>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="btnSubmit" class="btn btn-primary btn-sm">Actualizar</button>
    </div>
</form>

<script>
    
    var campos = ['producto','cantidad','precioUnitario','descuento','observacion'];
    $("#form-update-sale-detail").on('submit', function(e) {
        $("[name=btnSubmit]").attr('disabled',true);
        e.preventDefault();
        $('.divMensajeDeEspera').slideDown();
        var registerForm = $("#form-update-sale-detail");
        var formData = new FormData($("#form-update-sale-detail")[0]);
        $.each(campos, function( indice, valor ) {
            $("#"+valor+"-error").html( "" );
            $("[name="+valor+"]").removeClass('is-invalid').addClass('is-valid');
            $("select[name="+valor+"]").removeClass('is-invalid-select').addClass('is-valid-select').removeClass('select2-selection');
        });
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('sale.details.update',$detail->id)}}",
            type: "POST",
            data:formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("[name=btnSubmit]").attr('disabled',true)
            },
            success:function(data) {
                $('.divMensajeDeEspera').hide();
                $(".startab").hide();
                if(data.alerta) {
                    // toastr.error(data.mensaje);
                    $("[name=btnSubmit]").attr('disabled',false)
                }else if(data.success == '1') {
                    $("[name=btnSubmit]").attr('disabled',true)
                    window.location.reload();
                }else if(typeof(data.status) == "undefined"){
          
                }

            },
            error: function(data){
                $('.divMensajeDeEspera').hide();
                $("[name=btnSubmit]").attr('disabled',false);
                if(data.responseJSON.errors) {
                    var contErrors = 0;
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#form-update-sale-detail #'+index+'-error' ).html( value );
                        $("#form-update-sale-detail [name="+index+"]").removeClass('is-valid').addClass('is-invalid');
                        $("#form-update-sale-detail select[name="+index+"]").removeClass('is-valid-select').addClass('is-invalid-select').removeClass('select2-selection');
                   
                        if (contErrors == 0) {
                            var divPadre1 = $("#form-update-sale-detail [name="+index+"]").closest('div.col-xs-12')[0];
                            var divPadre2 = $("#form-update-sale-detail [name="+index+"]").closest('div')[0];
                            if(divPadre1 != null){
                                divPadre1.scrollIntoView({behavior: 'smooth'});
                            }else if(divPadre2 != null){
                                divPadre2.scrollIntoView({behavior: 'smooth'});
                            }
                        }
                        contErrors++;
                    });
                }
                if(typeof(data.status) != "undefined" && data.status != null && data.status == '401'){
                    window.location.reload();
                }
            }
        });
    });

    // AutoNumeric
    $(document).ready(function () {
        new AutoNumeric('#precioUnitario', {
            minimumValue: 0,
            decimalPlaces: 2
        });
        new AutoNumeric('#descuento', {
            minimumValue: 0,
            decimalPlaces: 2
        });
        new AutoNumeric('#cantidad', {
            minimumValue: 0,
            decimalPlaces: 0
        });
    });
</script>