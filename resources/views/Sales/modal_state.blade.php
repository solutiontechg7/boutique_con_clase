<form method="POST" action="{{ route('sales.updateState',$sale->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cambiar de estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12">
            <div class="row text-center">
                <div class="col-md-6" style="font-size: 15px;">
                    Validar <input type="radio" class="value" name="estado" id="estado" value="1" checked>
                </div>
                <div class="col-md-6" style="font-size: 15px;">
                    Anular <input type="radio" class="value" name="estado" id="estado" value="2">
                </div>
                <br>
                <div class="col-md-12 text-center" style="font-size: 14px;">
                    <br>
                    ¿Estó seguro de cambiar de estado?
                    <br><br>
                </div>
            </div>
        </div> 
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-sm">Cambiar</button>
    </div>
</form>