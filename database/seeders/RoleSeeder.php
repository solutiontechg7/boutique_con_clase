<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name'=>'Admin']);
        $role2 = Role::create(['name'=>'Ventas']);

        Permission::create(['name'=>'users.index'])->syncRoles([$role1]);
        Permission::create(['name'=>'users.admin'])->syncRoles([$role1]);

        Permission::create(['name'=>'products.index'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'products.admin'])->syncRoles([$role1]);

        Permission::create(['name'=>'providers.index'])->syncRoles([$role1]);
        Permission::create(['name'=>'providers.admin'])->syncRoles([$role1]);

        Permission::create(['name'=>'clients.index'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'clients.admin'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'clients.destroy'])->syncRoles([$role1]);

        Permission::create(['name'=>'shopping.index'])->syncRoles([$role1]);
        Permission::create(['name'=>'shopping.admin'])->syncRoles([$role1]);

        Permission::create(['name'=>'sales.index.my'])->syncRoles([$role1]);
        Permission::create(['name'=>'sales.index.all'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'sales.admin.my'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'sales.admin.all'])->syncRoles([$role1]);
        Permission::create(['name'=>'sales.admin.detail'])->syncRoles([$role1,$role2]);
        Permission::create(['name'=>'sales.destroy.all'])->syncRoles([$role1]);


    }
}
